package com.tieto.tds.services;

import java.time.LocalDate;
import java.util.Date;

public interface DateTimeService {

	public FromToDateHelper getStartAndEndDayOfMonthForThisDay(Date thisDay);

	public FromToDateHelper getStartAndEndDayOfLast30Days();

	/**
	 * 
	 * @param page : 0 represents last 30 day, 
	 * any positive number means months in history from now
	 */
	public FromToDateHelper getStartAndEndDayByPage(int page);

	public String getInvoiceNumberFromStartDate(LocalDate startDate);
}
