package com.tieto.tds.repositories;

import java.time.LocalDate;
import java.util.List;

import com.tieto.tds.dtos.company.project.app.ApplicationDto;

public interface ApplicationRepository {

	public List<ApplicationDto> findAllActiveAppsByProjectidAndPeriod(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo);
}
