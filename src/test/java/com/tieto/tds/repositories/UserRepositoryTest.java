package com.tieto.tds.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.repositories.impls.UserRepositoryImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserRepositoryTest {

	
	@Test
	public void test_getAllUsersByCompanyidAndCostCenterTitleUrl_works(){

		PortalProperties properties = new PortalProperties();
		properties.setSchema("http");
		properties.setFqdn("test.com");
		UserRepositoryImpl repository = new UserRepositoryImpl(null, properties);

		String url = repository.getAllUsersByCompanyidAndCostCenterTitleUrl("222", "111");

		assertThat(url).isEqualTo("http://test.com/rest-api/areas/222/costcenters/111/users?max=100000");

	}
}
