package com.tieto.tds.repositories.impls;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.repositories.ResourceRepository;

import edu.emory.mathcs.backport.java.util.Arrays;

@Repository
public class ResourceRepositoryImpl implements ResourceRepository{

	private static final Logger log = LoggerFactory.getLogger(ProjectRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public ResourceRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}

	@Override
	public List<ResourceDto> findAllByCompanyAndProject(String companyid, String projectid) {
		String url = findAllResouresUrl(companyid, projectid);

		ResourceDto[] resources = restTemplate.getForObject(url, ResourceDto[].class);
		return Arrays.asList(resources);
	}

	private String findAllResouresUrl(String companyid, String projectid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{areaid}/products/{projectid}/resources").buildAndExpand(companyid, projectid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

}
