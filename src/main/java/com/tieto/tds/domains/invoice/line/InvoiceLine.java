package com.tieto.tds.domains.invoice.line;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.tieto.tds.domains.common.DbEntity;
import com.tieto.tds.domains.invoice.Invoice;

@Entity(name="b_invoice_line")
public class InvoiceLine extends DbEntity{

	private static final long serialVersionUID = -1188947723694867780L;

	@ManyToOne
	private Invoice invoice;

	@Column(name="itemname")
	private String itemName;

	@Column
	private String unit;

	@Column(precision=20, scale=10)
	private Double amount = 0.0;

	@Column(precision=20, scale=10)
	private BigDecimal price = BigDecimal.ZERO;

	@Column(name="dateureated")
	private Date dateCreated = new Date();

	@Column(name="lastupdated")
	private Date lastUpdated;

	public InvoiceLine() {
		super();
		// for hibernate
	}

	public InvoiceLine(Invoice invoice, String itemName, String unit, Double amount, BigDecimal price) {
		super();
		this.invoice = invoice;
		this.itemName = itemName;
		this.unit = unit;
		this.amount = amount;
		this.price = price;
	}
	
	public InvoiceLine(Invoice invoice, String itemName, String unit, Double amount, double price) {
		super();
		this.invoice = invoice;
		this.itemName = itemName;
		this.unit = unit;
		this.amount = amount;
		this.price = BigDecimal.valueOf(price);
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getPriceRounded() {
		return price.setScale(2, RoundingMode.DOWN);
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return itemName + ", price: " + price + ", amount: " + amount + " " + unit;
	}

	public void addAmount(double amountToAdd){
		this.amount += amountToAdd;
	}
}
