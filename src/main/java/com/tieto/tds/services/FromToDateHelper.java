package com.tieto.tds.services;

import java.util.Date;

public class FromToDateHelper {

	private Date dateFrom;

	private Date dateTo;

	public FromToDateHelper(Date dateFrom, Date dateTo) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public long getDateFromTime(){
		return dateFrom.getTime();
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public long getDateToTime(){
		return dateTo.getTime();
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public long getFromToDifference(){
		return getDateToTime()-getDateFromTime();
	}

	@Override
	public String toString() {
		return dateFrom +" - "+dateTo;
	}

}
