package com.tieto.tds.services.reports;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.reporting.CapacityStatisticsDto;

@RunWith(MockitoJUnitRunner.class)
public class StatisticServiceTest {

	@Mock
	private JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	private StatisticServiceImpl service = new StatisticServiceImpl();

	@Test
	public void testGetAllProjectsUsageForPeriod(){
		
		List<ProjectInvoiceExportLine> list = new ArrayList<>();
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "RAM", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "CPU", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "DISK", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "INSTANCES", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "Images", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "Snapshots", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "Volumes", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "HDD", 2d) );
		list.add( new ProjectInvoiceExportLine("2017-01", "222", "111", "not supported", 2d) );

		when(jdbcTemplate.query(anyString(), any(Object[].class), Matchers.<RowMapper<ProjectInvoiceExportLine>>any())).thenReturn(list);
		
		Map<String, CapacityStatisticsDto> usages = service.getAllProjectsUsageForPeriod("2017-01");
		
		CapacityStatisticsDto capacityStatisticsDto = usages.get("222");
		
		assertThat("111").isEqualTo( capacityStatisticsDto.getCompanyid() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getRam() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getCpu() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getDisk() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getInstance() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getImage() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getSnapshot() );
		assertThat(2d).isEqualTo( capacityStatisticsDto.getVolume() );

	}

	@Test
	public void testTotalPrice(){
		StatisticServiceImpl service = new StatisticServiceImpl();
		
		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		lines.add(getInvoiceLine(1, 2976, "CPU", "2017-01", new BigDecimal("0.00877"), "pcs"));
		lines.add(getInvoiceLine(2, 5952, "RAM", "2017-01", new BigDecimal("0.00731"), "pcs"));
		lines.add(getInvoiceLine(3, 74400, "HDD", "2017-01", new BigDecimal("0.00015"), "pcs"));
		lines.add(getInvoiceLine(4, 1488, "instances", "2017-01", new BigDecimal("0.02792"), "pcs"));

		
		BigDecimal totalPrice = service.getTotalPrice(lines);
		
		assertThat(new BigDecimal("122.31360")).isEqualTo(totalPrice);
	}
	
	@Test
	public void testTotalPriceWithNoLines(){
		StatisticServiceImpl service = new StatisticServiceImpl();
		
		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		
		BigDecimal totalPrice = service.getTotalPrice(lines);
		
		assertThat(new BigDecimal("0")).isEqualTo(totalPrice);
	}
	
	@Test
	public void testTotalPriceWithNullLines(){
		StatisticServiceImpl service = new StatisticServiceImpl();
		
		BigDecimal totalPrice = service.getTotalPrice(null);
		
		assertThat(new BigDecimal("0")).isEqualTo(totalPrice);
	}
	
	private InvoiceLine getInvoiceLine(long id, double amount, String itemName, String no, BigDecimal price, String unit) {
		InvoiceLine line1 = new InvoiceLine();
		line1.setAmount(amount);
		line1.setDateCreated(Calendar.getInstance().getTime());
		line1.setId(id);
		line1.setItemName(itemName);
		line1.setLastUpdated(Calendar.getInstance().getTime());
		line1.setPrice(price);
		line1.setUnit(unit);
		return line1;
	}

}
