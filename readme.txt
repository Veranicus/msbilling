This is a microserice for encapsulating billing functionality
it use cas-user header for authentication

HOW TO RUN BUILDED APPLICATION
sudo nohup java -jar {buildedApplication}.jar

additional params which could be used after -jar:
--spring.config.location="file:///c:/programs/apache-tomcat-8.5.6/conf/ldap.properties" - set location of externalized property file
-Dspring.profiles.active={selectedProfile} - set spring profile to be used which will load specific set of beans

HOW TO STOP RUNNING APPLICATION
curl -X POST --header "cas-user: admin" http://localhost:8190/application/shutdown

HOW TO READ LOGS
sudo tail -f -n 1000 /var/log/tdscat/msbilling.log

CONFIG LOCATION
/opt/portal/microservices/billing/msbilling.properties

HEALTH ACTUATOR URL
http://localhost:8091/actuator/health

DOCKER KNOW HOW
copy target\billing-0.0.1-SNAPSHOT.jar docker\app.jar
docker build -t microservice/billing docker

docker stop microservice/billing
docker rm microservice/billing
docker run -P -d microservice/billing --name billing

docker run -itd -p 80:80 --name=discovery microservice/discovery /bin/bash

expose extenal port 90 to inner port 80, name container as discovery and start image microservice/billing
docker run -itd -p 90:80 --name=discovery microservice/discovery /bin/bash

docker run -itd -p 90:8080 --link=discovery --name=project microservice/project /bin/bash

docker run -itd -p 8080 --link=discovery --name=bacovjir microservice/bacovjir /bin/bash

Code analysis
To run code analysis on SonarCube run these maven tasks:
1. clean test
2. sonar:sonar -Dsonar.host.url=https://sonarlts.shared.pub.tdsdev.tieto.com
ID of sonar cube project is set in pom.xml