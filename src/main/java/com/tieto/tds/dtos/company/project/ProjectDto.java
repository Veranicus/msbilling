package com.tieto.tds.dtos.company.project;

import java.util.Date;
import java.util.Set;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.user.UserDto;

public class ProjectDto {

	//DB ID
	private long id;
	//alternate key
	private String productId;

	private String name;
	private Date dateCreated;
	private UserDto createdBy;
	private Date dateDeleted;
	private UserDto deletedBy;
	private CostCenterDto costCenter;
	private String description;
	private String richDescription;
	private boolean active;
	private boolean createResource;

	private Set<String> uiElementIDs;

	private TierDto tier;

	private String companyid;

	private CompanyDto company;

	private boolean massManagement;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getUiElementIDs() {
		return uiElementIDs;
	}

	public void setUiElementIDs(Set<String> uiElementIDs) {
		this.uiElementIDs = uiElementIDs;
	}

	public String getDescription() {
		if (description == null){
			return "";
		}
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public UserDto getCreatedBy() {
		if (createdBy == null){
			return new UserDto();
		}
		return createdBy;
	}

	public void setCreatedBy(UserDto createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public UserDto getDeletedBy() {
		if (deletedBy == null){
			return new UserDto();
		}
		return deletedBy;
	}

	public void setDeletedBy(UserDto deletedBy) {
		this.deletedBy = deletedBy;
	}

	public TierDto getTier() {
		if (tier != null){
			return tier;
		} else {
			return new TierDto(1, false, "NA", null);
		}
	}

	public void setTier(TierDto tier) {
		this.tier = tier;
	}

	@Override
	public String toString() {
		return "PS dto = " + getName() + " - " + getProductId() + ", " + getDescription();
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public CompanyDto getCompany() {
		if (company != null){
			return company;
		} else {
			return new CompanyDto("NA", "NA", "NA", 0d, "NA", 0d, "NA");
		}
	}

	public void setCompany(CompanyDto company) {
		this.company = company;
	}

	public boolean isCreateResource() {
		return createResource;
	}

	public void setCreateResource(boolean createResource) {
		this.createResource = createResource;
	}

	public boolean isMassManagement() {
		return massManagement;
	}

	public void setMassManagement(boolean massManagement) {
		this.massManagement = massManagement;
	}

	public CostCenterDto getCostCenter() {
		if (costCenter != null) {
			return costCenter;
		} else {
			return new CostCenterDto(0, "NA", null, false);
		}
	}

	public void setCostCenter(CostCenterDto costCenter) {
		this.costCenter = costCenter;
	}

	public String getRichDescription() {
		return richDescription;
	}

	public void setRichDescription(String richDescription) {
		this.richDescription = richDescription;
	}

}
