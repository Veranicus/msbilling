package com.tieto.tds.services;

import java.math.BigDecimal;

public interface MathService {

	/**
	 * 
	 * @param price
	 * @return rounded number to 2 decimal place half down
	 */
	public BigDecimal getRoundedNumber(BigDecimal price);
}
