package com.tieto.tds.services.reports;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;

public interface ReportHeaderService {

	public Row createUsageHeader(Row row);
	
	public Row createProjectHeader(Row row);
	
	public Row createHeaderForCostCenters(Row row);
	
	public Row createHeaderForCompany(Row row);

	public void createCostCenterFullReportHeader(Row row, Row subheader, CompanyDto company, List<AurApp> aurApps);
}
