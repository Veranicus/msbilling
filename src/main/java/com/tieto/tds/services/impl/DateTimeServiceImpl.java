package com.tieto.tds.services.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.services.DateTimeService;
import com.tieto.tds.services.DateUtils;
import com.tieto.tds.services.FromToDateHelper;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class DateTimeServiceImpl implements DateTimeService{

	@Override
	public FromToDateHelper getStartAndEndDayOfMonthForThisDay(Date thisDay) {
		Date dateTo  = DateUtils.getLastDayOfPreviousMonth(thisDay);
		Date dateFrom = DateUtils.getFirstDayOfMonth(thisDay);
		return new FromToDateHelper(dateFrom, dateTo);
	}

	@Override
	public FromToDateHelper getStartAndEndDayOfLast30Days() {
		Date dateTo;
		Date dateFrom;
		Calendar cal = Calendar.getInstance();
		dateTo = cal.getTime();
		cal.add(Calendar.DAY_OF_YEAR, -30);
		dateFrom = cal.getTime();
		return new FromToDateHelper(dateFrom, dateTo);
	}

	@Override
	public FromToDateHelper getStartAndEndDayByPage(int page) {
		Date dateTo;
		Date dateFrom;
		Calendar cal = Calendar.getInstance();
		if (page==0){
			dateTo = cal.getTime();
			cal.add(Calendar.DAY_OF_YEAR, -30);
			dateFrom = cal.getTime();
		} else {
			cal.add(Calendar.MONTH, -page);
			dateFrom = DateUtils.getFirstDayOfMonth(cal.getTime());
			dateTo = DateUtils.getLastDayOfPreviousMonth(cal.getTime());
		}
		return new FromToDateHelper(dateFrom, dateTo);
	}

	@Override
	public String getInvoiceNumberFromStartDate(LocalDate startDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String invoiceNumber = startDate.format(formatter);
		return invoiceNumber;
	}

}
