package com.tieto.tds.controllers;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tieto.tds.dtos.invoicing.CompanyInvoiceDto;
import com.tieto.tds.services.CompanyInvoiceService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.commons.LocalDateHolder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags={"Company"})
public class CompanyBillingController {

	private static final Logger log = LoggerFactory.getLogger(CompanyBillingController.class);

	private CompanyInvoiceService companyInvoiceService;

	private LocalDateUtilService localDateUtilService;

	@Autowired
	public CompanyBillingController(CompanyInvoiceService companyInvoiceService,
			LocalDateUtilService localDateUtilService) {
		super();
		this.companyInvoiceService = companyInvoiceService;
		this.localDateUtilService = localDateUtilService;
	}

	@ApiOperation(value = "Create project invoice for specific period")
	@RequestMapping(path="/company/{companyid}/invoice", method = RequestMethod.POST)
	public CompanyInvoiceDto createProjectInvoiceByDate(
			@PathVariable("companyid") String companyid,
			@RequestParam(value="from", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodFrom,
			@RequestParam(value="to", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodTo){
		log.info("periodFrom = {}, periodTo = ", periodFrom, periodTo);

		LocalDateHolder localDateHolder = localDateUtilService.getFromAndToFromInput(periodFrom, periodTo);
		
		return companyInvoiceService.createInvoice(companyid, localDateHolder.getFrom(), localDateHolder.getTo());
	}

	@ApiOperation(value = "Get project invoice by id")
	@RequestMapping(path="/company/{companyid}/invoice/{id}", method = RequestMethod.GET)
	public CompanyInvoiceDto findOneCompanyInvoiceById(
			@PathVariable("companyid") String companyid,
			@PathVariable("id") long id){
		return companyInvoiceService.findOne(companyid, id);
	}
	
	@ApiOperation(value = "get all project invoice")
	@RequestMapping(path="/company/{companyid}/invoice", method = RequestMethod.GET)
	public List<CompanyInvoiceDto> findAllCompanyInvoices(
			@PathVariable("companyid") String companyid,
			@RequestParam(value="max", required=false, defaultValue="10") String max,
			@RequestParam(value="page", required=false, defaultValue="1") String page){
		log.debug("/company/{}/invoice?max={}&page={}", companyid, max, page);

		return companyInvoiceService.findAllByCompanyid(companyid);
	}
}
