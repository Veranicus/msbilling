package com.tieto.tds.services.reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.domains.invoice.CompanyInvoice;
import com.tieto.tds.domains.invoice.CostCenterInvoice;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.reporting.CapacityStatisticsDto;
import com.tieto.tds.events.GenerateReportEvent;
import com.tieto.tds.exceptions.PDCException;
import com.tieto.tds.repositories.AurRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.CostCenterRepository;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.invoicing.CompanyInvoiceRepository;
import com.tieto.tds.repositories.invoicing.CostCenterInvoiceRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.repositories.invoicing.ProjectInvoiceRepository;
import com.tieto.tds.services.MailService;
import com.tieto.tds.services.MathService;
import com.tieto.tds.services.UrlService;

@Service
@Profile("default")
@Transactional(isolation=Isolation.READ_UNCOMMITTED)
public class ExportInvoiceServiceImpl implements ExportInvoiceService, ApplicationListener<GenerateReportEvent>{

	private static final Logger log = LoggerFactory.getLogger(ExportInvoiceServiceImpl.class);

	private static final String USER_HOME = System.getProperty("user.home");

	private static final String FILE_SEPARATOR = System.getProperty("file.separator");

	private CompanyRepository companyRepository;

	private CompanyInvoiceRepository companyInvoiceRepository;

	private StatisticService statisticService;

	private PortalProperties properties;
	
	private ProjectInvoiceRepository projectInvoiceRepository;
	
	private CostCenterInvoiceRepository costCenterInvoiceRepository;

	private ProjectRepository projectRepository;
	
	private CostCenterRepository costCenterRepository;

	private AurRepository aurRepository;

	private InvoiceLineRepository invoiceLineRepository;

	private ExportInvoiceHelperService exportInvoiceHelperService;

	private MailService mailService;

	private MathService mathService;

	private UrlService urlService;

	private ReportHeaderService reportHeaderService;

	@Autowired
	public ExportInvoiceServiceImpl(CompanyRepository companyRepository,
			StatisticService statisticService,
			PortalProperties properties,
			ProjectInvoiceRepository projectInvoiceRepository,
			ProjectRepository projectRepository,
			CostCenterInvoiceRepository costCenterInvoiceRepository,
			CostCenterRepository costCenterRepository,
			CompanyInvoiceRepository companyInvoiceRepository,
			AurRepository aurRepository,
			InvoiceLineRepository invoiceLineRepository,
			ExportInvoiceHelperService exportInvoiceHelperService,
			MailService mailService,
			MathService mathService,
			UrlService urlService,
			ReportHeaderService reportHeaderService) {
		super();
		this.companyRepository = companyRepository;
		this.statisticService = statisticService;
		this.properties = properties;
		this.projectInvoiceRepository = projectInvoiceRepository;
		this.projectRepository = projectRepository;
		this.costCenterInvoiceRepository = costCenterInvoiceRepository;
		this.costCenterRepository = costCenterRepository;
		this.companyInvoiceRepository = companyInvoiceRepository;
		this.aurRepository = aurRepository;
		this.invoiceLineRepository = invoiceLineRepository;
		this.exportInvoiceHelperService = exportInvoiceHelperService;
		this.mailService = mailService;
		this.mathService = mathService;
		this.urlService = urlService;
		this.reportHeaderService = reportHeaderService;
	}

	@Override
	public ByteArrayOutputStream exportInvoicesByPeriod(String period) {
		log.debug("exportInvoicesByPeriod start for period: {}", period);
		try {
			log.debug("exportUsageByPeriod started");
			XSSFWorkbook workbook = new XSSFWorkbook();

			addUsageReportTab(workbook, period);
			addProjectInvoicesTab(workbook, period);
			addCostCenterInvoicesTab(workbook, period);
			addCompanyInvoicesTab(workbook, period);
			addCostCenterFullReport(workbook, period);

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			workbook.write(stream);
			workbook.close();

			saveToFile(stream, period);

			log.debug("exportUsageByPeriod finished");
			return stream;
		} catch (IOException e) {
			log.error("Error exporting usage for period: ", e);
			throw new PDCException("Cannot export usage for period: " + e.getMessage());
		}
	}

	private void saveToFile(ByteArrayOutputStream stream, String period) throws IOException {
		try {
			String file = USER_HOME+FILE_SEPARATOR+"invoiceexport-"+properties.getFqdn().replaceAll(":", "")+"-"+period+".xls";
			log.debug("Saving invoice report to {}.", file);
			File filePath = new File(file);
			boolean createNewFile = filePath.createNewFile();
			if (createNewFile){
				log.debug("File {} was created", file);
			} else {
				log.warn("File {} already exists", file);
			}
			OutputStream outStream = new FileOutputStream(filePath);
			stream.writeTo(outStream);
		} catch (IOException e) {
			log.error("Cannot save invoice report to file.", e);
		}
	}

	private void addCostCenterFullReport(XSSFWorkbook workbook, String number) {
		log.debug("addCostCenterFullReport started");

		for (CompanyDto company : companyRepository.findAll()) {
			try {
				List<CostCenterInvoice> invoices = costCenterInvoiceRepository.findAllByCompanyidAndNumber(company.getAreaid(), number);
				List<AurApp> aurApps = aurRepository.findAllAurAppsByCompanyid(company.getAreaid());
				XSSFSheet sheet = workbook.createSheet("CC full report for " + company.getTitle());

				Row header1 = sheet.createRow(0);
				Row subheader = sheet.createRow(1);

				reportHeaderService.createCostCenterFullReportHeader(header1, subheader, company, aurApps);

				int i = 2;
				for (CostCenterInvoice invoice : invoices){
					log.debug("Add CC data: " + invoice.getCostCenterId() );
					Row row1 = sheet.createRow(i++);
					int saasCount = 2;
					try {
						List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(invoice);
						BigDecimal totalPrice = statisticService.getTotalPrice(lines);
						log.debug("CC totalPrice: {}", totalPrice);

						CostCenterDto costCenterDto = costCenterRepository.findOneCostCenter(invoice.getCompanyid(), invoice.getCostCenterId());

						String title = invoice.getCostCenterId();
						if (costCenterDto != null){
							title = costCenterDto.getTitle();
							if (title == null || title.length()==0){
								title = costCenterDto.toString();
							}
						}
						row1.createCell(0).setCellValue(title);						

						double userCount = exportInvoiceHelperService.getUserCountForCC(lines);
						row1.createCell(1).setCellValue(userCount);

						
						for (AurApp aurApp : aurApps) {
							double value = exportInvoiceHelperService.getAurUsersCount(aurApp, lines);
							row1.createCell(saasCount++).setCellValue(value);
						}

					} catch (Exception e){
						log.debug("could not add usage to excel row: " + e.getMessage(), e);
						try {
							row1.createCell(saasCount++).setCellValue(invoice.getCostCenterId()
									+ " - " + e.getMessage());
						} catch (Exception ee){
							log.warn("Could not log usage error. ", ee);
							row1.createCell(saasCount++).setCellValue(e.getMessage());
						}
					}
				}
			} catch (Exception e){
				log.error("Cannot generate CC report in excel. ", e);
			}
		}

	}

	private void addUsageReportTab(XSSFWorkbook workbook, String period){
		log.debug("addUsageReportTab started");

		Map<String, CapacityStatisticsDto> usage = statisticService.getAllProjectsUsageForPeriod(period);

		XSSFSheet sheet = workbook.createSheet("Usage report");

		Row header1 = sheet.createRow(0);
		reportHeaderService.createUsageHeader(header1);

		int i = 1;
		for (Map.Entry<String, CapacityStatisticsDto> entry : usage.entrySet()){
			log.debug("Add Usage data: "+ entry.getKey() + "/" + entry.getValue());
			CapacityStatisticsDto capacity = entry.getValue();
			Row row1 = sheet.createRow(i++);
			StringBuilder sb = new StringBuilder();
			try {
				String projectName = entry.getKey();
				try {
					log.debug("find Project detail by {}, {}.", entry.getValue().getCompanyid(), entry.getValue().getProjectid() );
					ProjectDto projectDto = projectRepository.findOneProject( entry.getValue().getCompanyid(), entry.getValue().getProjectid());
					projectName = projectDto.getName();
				} catch (Exception e){
					sb.append("Cannot get project detail from mediator:");
					sb.append(e.getStackTrace());
					log.warn("Cannot get project details for usage report. ", e);
				}
				row1.createCell(0).setCellValue(projectName);
				row1.createCell(1).setCellValue(entry.getKey());
				row1.createCell(2).setCellValue(capacity.getCpu());
				row1.createCell(3).setCellValue(capacity.getRam());
				row1.createCell(4).setCellValue(capacity.getDisk());
				row1.createCell(5).setCellValue(capacity.getInstance());
				row1.createCell(6).setCellValue(capacity.getSnapshot());
				row1.createCell(7).setCellValue(capacity.getImage());
				row1.createCell(8).setCellValue(capacity.getVolume());
			} catch (Exception e){
				sb.append("Cannot generate usage reports:");
				sb.append(e.getStackTrace());
				log.debug("could not add usage to excel row: " + e.getMessage(), e);
			}
			try {
				row1.createCell(9).setCellValue(sb.toString());
			} catch (Exception e){
				log.error("Error writing error message to cell: ", e);
			}
		}
	}

	private void addProjectInvoicesTab(XSSFWorkbook workbook, String number) {
		log.debug("addProjectInvoicesTab started {}", number);

		XSSFSheet projectInvoicesSheet = workbook.createSheet("Project Invoices");
		XSSFSheet projectInvoicesZeroSheet = workbook.createSheet("Project Invoices Zero");
		Row invoiceHeader = projectInvoicesSheet.createRow(0);
		Row invoiceZeroHeader = projectInvoicesZeroSheet.createRow(0);
		reportHeaderService.createProjectHeader(invoiceHeader);
		reportHeaderService.createProjectHeader(invoiceZeroHeader);

		List<ProjectInvoice> projectInvoices = projectInvoiceRepository.findAllByNumber(number);
		int j = 1;
		int k = 1;
		for (ProjectInvoice invoice: projectInvoices){
			log.debug("add project invoice: " + invoice.getId());
			Row row;
			List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(invoice);
			BigDecimal totalPrice = statisticService.getTotalPrice(lines);
			if (totalPrice.compareTo(BigDecimal.ZERO) == 0){
				row = projectInvoicesZeroSheet.createRow(j++);
			} else {
				row = projectInvoicesSheet.createRow(k++);
			}
			try {
				if (invoice.getProjectid() != null && invoice.getCompanyid() != null){
					ProjectDto project = projectRepository.findOneProject(invoice.getCompanyid(), invoice.getProjectid());
					CompanyDto company = companyRepository.findOneCompany(invoice.getCompanyid());
					row.createCell(0).setCellValue(project.getName());
					row.createCell(1).setCellValue(project.getProductId());
					row.createCell(2).setCellValue(project.getCostCenter().getTitle());
					row.createCell(4).setCellValue(project.getTier().getTitle());
					row.createCell(5).setCellValue(company.getTitle());
	
					String projectUrl = urlService.getProjectUrl(project);
					Cell cell = row.createCell(6);
					createHyperLink(workbook, cell, projectUrl);
				}

				BigDecimal value = mathService.getRoundedNumber(totalPrice);
				row.createCell(3).setCellValue(value.doubleValue());
			} catch (Exception e){
				log.warn("Cannot add invoice to xls.", e);
				row.createCell(7).setCellValue("cannot add invoice " + invoice.toString() + e.getMessage());
			}
		}
	}

	private void createHyperLink(XSSFWorkbook workbook, Cell cell, String projectUrl) {
		try {
			CreationHelper createHelper = workbook.getCreationHelper();
			XSSFHyperlink url = (XSSFHyperlink) createHelper.createHyperlink(XSSFHyperlink.LINK_URL);

			XSSFCellStyle hyperlinkStyle = workbook.createCellStyle();
			XSSFFont hlinkFont = workbook.createFont();
			hlinkFont.setColor(IndexedColors.BLUE.getIndex());
			hyperlinkStyle.setFont(hlinkFont);


			url.setAddress(projectUrl);
			try {
				cell.setHyperlink(url);
			} catch (NullPointerException e) {
				log.warn("Error adding url {}", projectUrl, e);
				cell.setCellType(XSSFCell.CELL_TYPE_STRING);
				cell.setHyperlink(url);
			}
			cell.setCellValue("link");
			cell.setCellStyle(hyperlinkStyle);
		} catch (Exception e){
			log.error("Could not create hyper link. ", e);
		}
	}

	private void addCostCenterInvoicesTab(XSSFWorkbook workbook, String period) {
		log.debug("addCostCenterInvoicesTab started");
		XSSFSheet ccInvoiceSheet = workbook.createSheet("CC Invoices ");
		XSSFSheet ccInvoiceZeroSheet = workbook.createSheet("CC Invoices ZERO ");
		Row invoiceHeader = ccInvoiceSheet.createRow(0);
		Row invoiceZeroHeader = ccInvoiceZeroSheet.createRow(0);
		reportHeaderService.createHeaderForCostCenters(invoiceHeader);
		reportHeaderService.createHeaderForCostCenters(invoiceZeroHeader);

		List<CostCenterInvoice> costCenterInvoiceDto = costCenterInvoiceRepository.findAllByNumber(period);
		int j = 1;
		int i = 1;
		for (CostCenterInvoice costCenterInvoice: costCenterInvoiceDto){
			log.debug("add CC invoice: " + costCenterInvoice.getId());
			Row row;
			List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(costCenterInvoice);
			BigDecimal totalPrice = statisticService.getTotalPrice(lines);
			if (totalPrice.compareTo(BigDecimal.ZERO) == 0){
				row = ccInvoiceZeroSheet.createRow(j++);
			} else {
				row = ccInvoiceSheet.createRow(i++);
			}

			try {
				if (costCenterInvoice.getCostCenterId() != null){
					CostCenterDto costCenter = costCenterRepository.findOneCostCenter(costCenterInvoice.getCompanyid(), costCenterInvoice.getCostCenterId());
					CompanyDto company = companyRepository.findOneCompany(costCenterInvoice.getCompanyid());

					row.createCell(0).setCellValue(costCenter.getTitle());
					row.createCell(1).setCellValue(company.getTitle());
					String ccUrl = urlService.getCostCenterUrl(costCenter);
					Cell cell = row.createCell(2);
					createHyperLink(workbook, cell, ccUrl);
	
				}
				BigDecimal value = totalPrice.setScale(2, RoundingMode.CEILING);
				row.createCell(2).setCellValue(value.doubleValue());
			} catch (Exception e){
				log.warn("Error adding price to CC invoice: {}", costCenterInvoice, e);
				row.createCell(3).setCellValue("cannot add invoice " + costCenterInvoice.toString() + e.getMessage());
			}
		}
	}

	private void addCompanyInvoicesTab(XSSFWorkbook workbook, String number) {
		log.debug("addCompanyInvoicesTab started");
		XSSFSheet companyInvoiceSheet = workbook.createSheet("Company Invoices ");
		XSSFSheet companyInvoiceZeroSheet = workbook.createSheet("Company Invoices ZERO ");
		Row invoiceHeader = companyInvoiceSheet.createRow(0);
		Row invoiceZeroHeader = companyInvoiceZeroSheet.createRow(0);
		reportHeaderService.createHeaderForCompany(invoiceHeader);
		reportHeaderService.createHeaderForCompany(invoiceZeroHeader);

		List<CompanyInvoice> companyInvoices = companyInvoiceRepository.findAllByNumber(number);
		int j = 1;
		int i = 1;
		for (CompanyInvoice customerInvoice: companyInvoices){
			Row row2;
			List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(customerInvoice);
			BigDecimal totalPrice = statisticService.getTotalPrice(lines);
			if (totalPrice.compareTo(BigDecimal.ZERO) == 0){
				row2 = companyInvoiceZeroSheet.createRow(j++);
			} else {
				row2 = companyInvoiceSheet.createRow(i++);
			}

			if (customerInvoice.getCompanyid() != null){
				CompanyDto company = companyRepository.findOneCompany(customerInvoice.getCompanyid());
				row2.createCell(0).setCellValue(company.getTitle());

				String companyUrl = urlService.getCompanyUrl(company);
				Cell cell = row2.createCell(2);
				createHyperLink(workbook, cell, companyUrl);

			}
			BigDecimal value = totalPrice.setScale(2, RoundingMode.CEILING);
			row2.createCell(1).setCellValue(value.doubleValue());
		}
	}

	@Override
	public void onApplicationEvent(GenerateReportEvent event) {
		log.debug("GenerateReportEvent onApplicationEvent entered with period: " + event.getPeriod());
		ByteArrayOutputStream byteArrayOutputStream = exportInvoicesByPeriod(event.getPeriod());

		try{
			String filePath = "/tmp/";
			String fileName = "invoice_report_"+event.getPeriod()+"_"+properties.getFqdn()+".xls";

			OutputStream outputStream = new FileOutputStream(filePath+fileName);
		    byteArrayOutputStream.writeTo(outputStream);

		    mailService.sendBillingReport(new File(filePath+fileName));
		} catch (Exception e){
			log.error("Error sending billing report.", e);
		}
	}
}
