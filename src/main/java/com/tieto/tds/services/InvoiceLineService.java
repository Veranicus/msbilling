package com.tieto.tds.services;

import java.math.BigDecimal;

import com.tieto.tds.domains.invoice.Invoice;

public interface InvoiceLineService {

	public BigDecimal getTotalPriceByInvoiceID(long id);

	public BigDecimal getTotalPriceByInvoiceInvoice(Invoice invoice);

}
