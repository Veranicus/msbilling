package com.tieto.tds.repositories;

import java.time.LocalDate;
import java.util.List;

import com.tieto.tds.dtos.company.project.resource.Usage;


public interface TenantUsageRepository {

	public List<Usage> getUsageForTenantInTimePeriod(String tenantid, LocalDate dateFrom, LocalDate dateTo);
}
