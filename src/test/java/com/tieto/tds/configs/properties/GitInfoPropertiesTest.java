package com.tieto.tds.configs.properties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.configs.properties.MailProperties.Report;

@RunWith(MockitoJUnitRunner.class)
public class GitInfoPropertiesTest {


	@Test
	public void test(){
		GitInfoProperties prop = new GitInfoProperties();
		prop.setBranch("branch");
		prop.setBuildTime("1234");
		prop.setBuildUserEmail("mail");
		prop.setBuildUserName("username");
		prop.setCommitId("f34f34");
		prop.setCommitIdAbbrev("abvr");
		prop.setCommitMessageFull("Message");
		prop.setCommitTime("3345345");
		prop.setCommitUserEmail("mail");
		prop.setCommitUserName("username");
		prop.setDescribe("desc");
		prop.setDescribeShort("short");
		prop.setDirty("dirty");
		prop.setRemoteOriginUrl("remote");
		prop.setTags("tags");
		prop.setCommitMessageShort("short");

		assertThat("branch").isEqualTo(prop.getBranch());
		assertThat("1234").isEqualTo(prop.getBuildTime());
		assertThat("mail").isEqualTo(prop.getBuildUserEmail());
		assertThat("username").isEqualTo(prop.getBuildUserName());
		assertThat("f34f34").isEqualTo(prop.getCommitId());
		assertThat("abvr").isEqualTo(prop.getCommitIdAbbrev());
		assertThat("Message").isEqualTo(prop.getCommitMessageFull());
		assertThat("3345345").isEqualTo(prop.getCommitTime());
		assertThat("mail").isEqualTo(prop.getCommitUserEmail());
		assertThat("username").isEqualTo(prop.getCommitUserName());
		assertThat("desc").isEqualTo(prop.getDescribe());
		assertThat("short").isEqualTo(prop.getDescribeShort());
		assertThat("dirty").isEqualTo(prop.getDirty());
		assertThat("remote").isEqualTo(prop.getRemoteOriginUrl());
		assertThat("tags").isEqualTo(prop.getTags());
		assertThat("short").isEqualTo(prop.getCommitMessageShort());
	}

}
