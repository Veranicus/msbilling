package com.tieto.tds.services.project;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.services.project.impl.ProjectResourceInvoiceLineCalculator;

public abstract class ProjectInvoiceLineCalculator {

	private static final Logger log = LoggerFactory.getLogger(ProjectInvoiceLineCalculator.class);

	protected ProjectInvoiceLineCalculator successor;

    public void setSuccessor(ProjectInvoiceLineCalculator successor) {
        this.successor = successor;
    }

    public List<InvoiceLineDto> getInvoiceLines(ProjectInvoiceRequest request) {
    	List<InvoiceLineDto> invoiceLines = new ArrayList<InvoiceLineDto>();
    	if (successor != null){
    		invoiceLines.addAll( successor.getInvoiceLines(request) );
    	}
    	log.debug("getProjectInvoiceLines from: {} with request: {}.", this.getClass().getSimpleName(), request);
    	invoiceLines.addAll( getProjectInvoiceLines(request) );
    	return invoiceLines;
    }
    
	public abstract List<InvoiceLineDto> getProjectInvoiceLines(ProjectInvoiceRequest requet);
}
