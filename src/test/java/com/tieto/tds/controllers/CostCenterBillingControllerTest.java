package com.tieto.tds.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;

import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;
import com.tieto.tds.services.CostCenterInvoiceService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.commons.LocalDateHolder;

public class CostCenterBillingControllerTest {

	MockMvc mockMvc;

	@Mock
	private CostCenterInvoiceService costCenterInvoiceService;

	@Mock
	private LocalDateUtilService localDateUtilService;


	@InjectMocks
	private CostCenterBillingController controller = new CostCenterBillingController(
			costCenterInvoiceService, localDateUtilService);
	

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();
	}	

	@Test
	public void thatCreateCostCenterInvoicsAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(any(LocalDate.class), any(LocalDate.class))).thenReturn(value );

		mockMvc.perform(post("/company/111/costcenter/222/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatGetCostCenterInvoiceAPIReturns200() throws Exception {
		CostCenterInvoiceDto ccInvoiceDto = new CostCenterInvoiceDto();
		ccInvoiceDto.setCompanyid("111");
		when(costCenterInvoiceService.findOne(anyString(), anyString(), anyLong())).thenReturn(ccInvoiceDto );
		mockMvc.perform(get("/company/111/costcenter/222/invoice/333"))
		.andDo(print())
		.andExpect(status().isOk());
	}


	@Test
	public void thatGetAllCostCenterInvoiceAPIReturns200() throws Exception {
		CostCenterInvoicePageDto page = new CostCenterInvoicePageDto();
		when(costCenterInvoiceService.findAllByCostCenter(anyString(), anyString(), any(PageableMs.class))).thenReturn(page);
		mockMvc.perform(get("/company/111/costcenter/222/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}



	@Test
	public void thatGetAllCompanyCostCenterInvoiceAPIReturns200() throws Exception {
		CostCenterInvoicePageDto page = new CostCenterInvoicePageDto();
		when(costCenterInvoiceService.findAllByCompanyid(anyString(), any(PageableMs.class))).thenReturn(page);
		mockMvc.perform(get("/company/111/costcenter/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatGetAllCompanyCostCenterInvoiceWithEmptyNumberParamAPIReturns200() throws Exception {
		CostCenterInvoicePageDto page = new CostCenterInvoicePageDto();
		when(costCenterInvoiceService.findAllByCompanyid(anyString(), any(PageableMs.class))).thenReturn(page);
		mockMvc.perform(get("/company/111/costcenter/invoice?number="))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	public void thatGetAllCompanyCostCenterInvoiceWithNumberParamAPIReturns200() throws Exception {
		CostCenterInvoicePageDto page = new CostCenterInvoicePageDto();
		when(costCenterInvoiceService.findAllByCompanyidAndNo(anyString(), anyString(), any(PageableMs.class))).thenReturn(page);
		mockMvc.perform(get("/company/111/costcenter/invoice?number=2017-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}

}
