package com.tieto.tds.services;

import java.util.List;

import com.tieto.tds.dtos.invoicing.CustomInvoiceLineDto;

public interface CustomInvoiceLineService {

	public CustomInvoiceLineDto createCustomInvoiceLineForProject(
			String companyid, String projectid, CustomInvoiceLineDto customInvoiceLine);
	
	public CustomInvoiceLineDto findOneCustomInvoiceLine(
			String companyid, String projectid, long id);
	
	public List<CustomInvoiceLineDto> findAllCustomInvoiceLinesByProjectAndNo(
			String companyid, String projectid, String no);
}
