package com.tieto.tds.dtos.migration;

public class ProjectInvoiceMig extends InvoiceMig{

	String productid;

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}
}
