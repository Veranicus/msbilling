package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tieto.tds.domains.invoice.Invoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.InvoiceLineService;

@Service
public class InvoiceLineServiceImpl implements InvoiceLineService{

	private InvoiceLineRepository invoiceLineRepository;
	 
	@Autowired
	public InvoiceLineServiceImpl(InvoiceLineRepository invoiceLineRepository) {
		super();
		this.invoiceLineRepository = invoiceLineRepository;
	}

	@Override
	public BigDecimal getTotalPriceByInvoiceID(long id) {
		List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoiceId(id);
		BigDecimal totalPrice = getTotalPrice(lines);
		return totalPrice;
	}

	@Override
	public BigDecimal getTotalPriceByInvoiceInvoice(Invoice invoice) {
		List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(invoice);
		BigDecimal totalPrice = getTotalPrice(lines);
		return totalPrice;
	}

	private BigDecimal getTotalPrice(List<InvoiceLine> lines) {
		BigDecimal totalPrice = BigDecimal.ZERO;
		if (lines != null && lines.size()>0){
			for (InvoiceLine line : lines) {
				BigDecimal subTotal = line.getPrice().multiply(new BigDecimal(line.getAmount()));
				totalPrice = totalPrice.add(subTotal);
			}
		}
		return totalPrice;
	}
}
