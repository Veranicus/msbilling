package com.tieto.tds.services;

public interface CronService {

	public void keepBackendSession();

	public void generateInvoices();
}
