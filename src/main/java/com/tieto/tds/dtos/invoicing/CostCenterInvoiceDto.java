package com.tieto.tds.dtos.invoicing;

public class CostCenterInvoiceDto extends InvoiceDto {

	private String companyid;

	private String costCenterId;

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getCostCenterId() {
		return costCenterId;
	}

	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}

}
