package com.tieto.tds.services.reports;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.company.project.aur.AurApp;

@Service
public class ExportInvoiceHelperServiceImpl implements ExportInvoiceHelperService {

	@Override
	public double getUserCountForCC(List<InvoiceLine> lines) {
		double userCount = 0;
		if (lines!=null && !lines.isEmpty()){
			for (InvoiceLine line : lines) {
				if (line.getItemName().contains(Constants.COSTCENTER_USER_FEE)){
					userCount = line.getAmount();
				}
			}
		}
		return userCount;
	}

	@Override
	public double getAurUsersCount(AurApp aurApp, List<InvoiceLine> lines) {
		double value = 0;
		if (lines!=null && !lines.isEmpty()){
			for (InvoiceLine line : lines) {
				if (line.getItemName().contains(aurApp.getFqdn())){
					value = line.getAmount();
				}
			}
		}
		return value;
	}
}
