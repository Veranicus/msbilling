package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.user.UserDto;

public interface AurRepository {

	public List<UserDto> findAllUsersByCostcenterAndAppid(String companyid, String costcenterid, String appid);

	public List<UserDto> findAllUsersByProjectAndAppid(String companyid, String projectid, String appid);

	public List<AurApp> findAllAurAppsByCompanyid(String companyid);
}
