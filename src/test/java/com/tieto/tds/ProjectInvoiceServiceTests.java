package com.tieto.tds;

import org.dozer.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.company.project.TierDto;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.repositories.invoicing.ProjectInvoiceRepository;
import com.tieto.tds.services.DateTimeService;
import com.tieto.tds.services.ProjectInvoiceLineService;
import com.tieto.tds.services.impl.ProjectInvoiceServiceImpl;
import com.tieto.tds.services.project.ProjectInvoiceLineCalculator;
import com.tieto.tds.services.project.ProjectInvoiceRequest;
import com.tieto.tds.services.project.impl.ProjectApplicationCostInvoiceLineCalculator;
import com.tieto.tds.services.project.impl.ProjectCustomInvoiceLineCalculator;
import com.tieto.tds.services.project.impl.ProjectResourceInvoiceLineCalculator;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.junit4.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ProjectInvoiceServiceTests {

	@Mock
	private ProjectRepository projectRepository;
	@Mock
	private ProjectInvoiceRepository invoiceRepository;
	@Mock
	private InvoiceLineRepository invoiceLineRepository;
	@Mock
	private Mapper mapper;
	@Mock
	private DateTimeService dateTimeService;

	@Mock
	private ProjectInvoiceLineCalculator projectInvoiceLineCalculator;
	private List<ProjectInvoiceLineCalculator> projectInvoiceLineCalculators;

	@InjectMocks
	private ProjectInvoiceServiceImpl service;

	@Before
	public void setup() throws Exception {
		
	}

	@Test
	public void testCreateProjectInvoice() {
		projectInvoiceLineCalculators = Arrays.asList(projectInvoiceLineCalculator);

		service = new ProjectInvoiceServiceImpl(invoiceRepository,
				invoiceLineRepository, mapper, projectInvoiceLineCalculators,dateTimeService);

		List<InvoiceLineDto> invoiceLines = new ArrayList<InvoiceLineDto>();
		InvoiceLineDto line1 = new InvoiceLineDto();
		line1.setAmount(2d);
		line1.setItemName("test");
		line1.setNo("1");
		line1.setPrice(new BigDecimal(30));
		line1.setUnit("pcs");
		invoiceLines.add(line1);

		when(projectInvoiceLineCalculator.getInvoiceLines(any(ProjectInvoiceRequest.class))).thenReturn(invoiceLines);
		when(dateTimeService.getInvoiceNumberFromStartDate(any(LocalDate.class))).thenReturn("2017-10");

		ProjectInvoice savedProjectInvoice = new ProjectInvoice();
		savedProjectInvoice.setId(2);

		when(this.invoiceRepository.saveAndFlush(any(ProjectInvoice.class))).thenReturn(savedProjectInvoice );
		when(invoiceLineRepository.saveAndFlush(any(InvoiceLine.class))).thenReturn(new InvoiceLine());

		LocalDate start = LocalDate.ofYearDay(2017, 1);
		LocalDate end = LocalDate.now();

		ProjectInvoiceDto invoice = service.createProjectInvoice("111", "222", start, end);
		assertThat(invoice.getId()).isEqualTo(2);
		assertThat(invoice.getNumber()).isEqualTo("2017-10");
		assertThat(invoice.getLines().size()).isEqualTo(1);
		assertThat(invoice.getCompanyid()).isEqualTo("111");
		assertThat(invoice.getProjectid()).isEqualTo("222");
		org.junit.Assert.assertEquals(invoice.getTotalPrice(),new BigDecimal(60));
	}
}
