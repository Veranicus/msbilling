package com.tieto.tds.dtos.common;

public class PageableMs {

	private int page;
	private int max;
	private PageSort sort;
	private String orderby;

	public PageableMs(int page, int max, PageSort sort, String orderby) {
		super();
		this.page = page;
		this.max = max;
		this.sort = sort;
		this.orderby = orderby;
	}

	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public PageSort getSort() {
		return sort;
	}
	public void setSort(PageSort sort) {
		this.sort = sort;
	}
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	
}
