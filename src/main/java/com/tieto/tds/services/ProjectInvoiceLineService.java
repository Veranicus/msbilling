package com.tieto.tds.services;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;

public interface ProjectInvoiceLineService {

	public void cleanupProjectinvoice(ProjectInvoice projectInvoice);

	public List<InvoiceLineDto> getProjectResourcesInvoiceLines(List<ResourceDto> resources, FeeDto fee, LocalDate periodFrom, LocalDate periodTo);

	public List<InvoiceLineDto> getProjectApplicationCostInvoiceLines(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo);
	
	public List<InvoiceLineDto> getProjectCustomInvoiceLines(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo);
}
