package com.tieto.tds.dtos.company.project.aur;

public enum SaasFeeType {
	
	PER_SERVICE, PER_USER;
}
