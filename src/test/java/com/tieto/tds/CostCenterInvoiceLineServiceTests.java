package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.AurBillingDto;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.AurRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.services.CostCenterHelper;
import com.tieto.tds.services.UserService;
import com.tieto.tds.services.impl.CostCenterInvoiceLineServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CostCenterInvoiceLineServiceTests {

	@Mock
	private AurRepository aurRepository;
	@Mock
	private UserRepository userRepository;
	@Mock
	private CompanyRepository companyRepository;
	@Mock
	private UserService userService;
	@Mock
	private CostCenterHelper costCenterHelper;
	@InjectMocks
	CostCenterInvoiceLineServiceImpl service = new CostCenterInvoiceLineServiceImpl(aurRepository, userRepository, companyRepository, userService, costCenterHelper);

	@Test
    public void testGetSaaSInvoiceLines() {
    
		
		List<AurApp> aurApps = new ArrayList<AurApp>();
		AurApp app1 = new AurApp();
		app1.setFqdn("saas.tds.com");
		app1.setAppid("444");
		AurBillingDto billing = new AurBillingDto();
		billing.setType(SaasFeeType.PER_USER);
		billing.setValue(2d);
		app1.setBilling(billing );
		
		AurApp app2 = new AurApp();
		app1.setFqdn("saas2.tds.com");
		AurBillingDto billing2 = new AurBillingDto();
		billing2.setType(SaasFeeType.PER_SERVICE);
		billing2.setValue(2d);
		billing2.setUserSuppoertFee(2d);
		app2.setBilling(billing2 );
		
		AurApp app3 = new AurApp();
		app3.setFqdn("saas3.tds.com");
		app3.setAppid("555");
		AurBillingDto billing3 = new AurBillingDto();
		billing3.setType(SaasFeeType.PER_USER);
		billing3.setValue(3d);
		app3.setBilling(billing3 );
		
		aurApps.add(app1);
		aurApps.add(app2);
		aurApps.add(app3);

		when(this.aurRepository.findAllAurAppsByCompanyid(anyString())).thenReturn(aurApps );

		List<UserDto> users = new ArrayList<UserDto>();
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());

		when(costCenterHelper.isAplicableForCCInvoice(app1)).thenReturn(true);
		when(costCenterHelper.isAplicableForCCInvoice(app2)).thenReturn(false);
		when(costCenterHelper.isAplicableForCCInvoice(app3)).thenReturn(true);
		
		when(aurRepository.findAllUsersByCostcenterAndAppid(anyString(), anyString(), anyString())).thenReturn(users );
		List<InvoiceLineDto> saaSInvoiceLines = service.getSaaSInvoiceLines("111", "CC", "2017-01");
		
		BigDecimal resultPrice = BigDecimal.ZERO;
		double resultAmount = 0;
		for (InvoiceLineDto line: saaSInvoiceLines){
			resultPrice = resultPrice.add(line.getPrice());
			resultAmount = resultAmount+line.getAmount();
		}

		assertThat(saaSInvoiceLines.size()).isEqualTo(2);
		assertThat(saaSInvoiceLines.get(0).getPrice()).isEqualTo(new BigDecimal(2));
		assertThat(saaSInvoiceLines.get(1).getPrice()).isEqualTo(new BigDecimal(3));
		assertThat(resultAmount ).isEqualTo(10d);
		assertThat(resultPrice).isEqualTo(new BigDecimal(5));

	}
	
	@Test
	public void test_getSaaSUserFeeInvoiceLines(){
	
		CompanyDto company = new CompanyDto();
		company.setUserFee(2d);
		company.setFeeType("per-costcenter");
		when(companyRepository.findOneCompany(anyString())).thenReturn(company );
		
		
		List<UserDto> users = new ArrayList<UserDto>();
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());
		users.add(new UserDto());

		when(userRepository.findAllUsersByCompanyidAndCostCenterTitle(anyString(), anyString())).thenReturn(users);

		
		when(userService.getNonAdminUsersCount(users)).thenReturn(4);

		List<InvoiceLineDto> saaSUserFeeInvoiceLines = service.getSaaSUserFeeInvoiceLines("111", "CC");
		
		BigDecimal resultPrice = BigDecimal.ZERO;
		double resultAmount = 0;
		for (InvoiceLineDto line: saaSUserFeeInvoiceLines){
			resultPrice = resultPrice.add(line.getPrice());
			resultAmount = resultAmount+line.getAmount();
		}

		assertThat(saaSUserFeeInvoiceLines.size()).isEqualTo(1);
		assertThat(resultAmount ).isEqualTo(4d);
		assertThat(resultPrice).isEqualTo(new BigDecimal(2));

	}
	
	@Test
	public void test_getSaaSUserFeeInvoiceLines_forPerServiceFee(){
		CompanyDto company = new CompanyDto();
		company.setUserFee(2d);
		company.setFeeType("per-service");
		when(companyRepository.findOneCompany(anyString())).thenReturn(company );
		
		List<InvoiceLineDto> saaSUserFeeInvoiceLines = service.getSaaSUserFeeInvoiceLines("111", "CC");
		assertThat(saaSUserFeeInvoiceLines.size()).isEqualTo(0);
	}
}
