package com.tieto.tds.domains.invoice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.line.InvoiceLine;

@RunWith(MockitoJUnitRunner.class)
public class CostCenterInvoiceTest {

	@Test
	public void test(){
		CostCenterInvoice invoice = new CostCenterInvoice();
		invoice.setCompanyid("111");
		invoice.setCostCenterId("222");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		List<InvoiceLine> lines =  new ArrayList<InvoiceLine>();
		invoice.setLines(lines);
		invoice.setNumber("2017-01");

		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("222").isEqualTo(invoice.getCostCenterId());
		assertThat("2017-01").isEqualTo(invoice.getNumber());
		assertNotNull(invoice.getDateCreated());
		assertThat(0).isEqualTo(invoice.getLines().size());
		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("111").isEqualTo(invoice.getCompanyid());
	}

	@Test
	public void testToString(){
		CostCenterInvoice invoice = new CostCenterInvoice("111", "222", "2017-01");
		assertThat("ID:0, no: 2017-01, costCenter: 222").isEqualTo(invoice.toString());
	}
}
