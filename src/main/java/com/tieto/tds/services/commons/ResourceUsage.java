package com.tieto.tds.services.commons;

import java.math.BigDecimal;

public class ResourceUsage {

	private BigDecimal cpu = BigDecimal.ZERO;
	private BigDecimal ram = BigDecimal.ZERO;
	private BigDecimal hdd = BigDecimal.ZERO;
	private BigDecimal instances = BigDecimal.ZERO;
	private BigDecimal images = BigDecimal.ZERO;
	private BigDecimal snapshots = BigDecimal.ZERO;
	private BigDecimal volumes = BigDecimal.ZERO;

	public BigDecimal getCpu() {
		return cpu;
	}
	public void setCpu(BigDecimal cpu) {
		this.cpu = cpu;
	}
	public void addCpu(BigDecimal cpu) {
		this.cpu = this.cpu.add(cpu);
	}
	public BigDecimal getRam() {
		return ram;
	}
	public void setRam(BigDecimal ram) {
		this.ram = ram;
	}
	public void addRam(BigDecimal ram) {
		this.ram = this.ram.add(ram);
	}
	public BigDecimal getHdd() {
		return hdd;
	}
	public void setHdd(BigDecimal hdd) {
		this.hdd = hdd;
	}
	public void addHdd(BigDecimal hdd) {
		this.hdd = this.hdd.add(hdd);
	}
	public BigDecimal getInstances() {
		return instances;
	}
	public void setInstances(BigDecimal instances) {
		this.instances = instances;
	}
	public void addInstances(BigDecimal instances) {
		this.instances = this.instances.add(instances);
	}
	public BigDecimal getImages() {
		return images;
	}
	public void setImages(BigDecimal images) {
		this.images = images;
	}
	public void addImages(BigDecimal images) {
		this.images = this.images.add(images);
	}
	public BigDecimal getSnapshots() {
		return snapshots;
	}
	public void setSnapshots(BigDecimal snapshots) {
		this.snapshots = snapshots;
	}
	public void addSnapshots(BigDecimal snapshots) {
		this.snapshots = this.snapshots.add(snapshots);
	}
	public BigDecimal getVolumes() {
		return volumes;
	}
	public void setVolumes(BigDecimal volumes) {
		this.volumes = volumes;
	}
	public void addVolumes(BigDecimal volumes) {
		this.volumes = this.volumes.add(volumes);
	}

	@Override
	public String toString() {
		return String.format("CPU: %s, RAM: %s, HDD: %s, Instances: %s, Images: %s, Snapshots: %s, Volumes: %s",
				getCpu(), getRam(), getHdd(), getInstances(), getImages(), getSnapshots(), getVolumes());
	}
}
