package com.tieto.tds.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.repositories.AurRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.services.impl.CompanyInvoiceLineServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CompanyInvoiceLineServiceTest {

	@Mock
	private ProjectInvoiceService projectInvoiceService;
	@Mock
	private CostCenterInvoiceService costCenterInvoiceService;
	@Mock
	private UserRepository userRepository;
	@Mock
	private CompanyRepository companyRepository;
	@Mock
	private ProjectRepository projectRepository;
	@Mock
	private AurRepository aurRepository;
	@Mock
	private UserService userService;
	@Mock
	private InvoiceLineService invoiceLineService;
	@InjectMocks
	private CompanyInvoiceLineServiceImpl service = new CompanyInvoiceLineServiceImpl(
			projectInvoiceService, userRepository, companyRepository,
			projectRepository, aurRepository, userService, costCenterInvoiceService, invoiceLineService);

	@Test
	public void thatGetProjectsInvoiceLinesReturnLines(){
		
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		List<ProjectInvoiceDto> content = new ArrayList<>();
		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		invoice.setProjectid("222");
		List<InvoiceLineDto> lines = new ArrayList<>();
		InvoiceLineDto line1 = new InvoiceLineDto();
		line1.setAmount(1d);
		line1.setPrice(new BigDecimal(100));
		lines.add(line1);
		InvoiceLineDto line2 = new InvoiceLineDto();
		line2.setAmount(1d);
		line2.setPrice(new BigDecimal(100));
		lines.add(line2);
		invoice.setLines(lines );
		content.add(invoice);
		page.setContent(content );
		
		when(projectInvoiceService.findAllByCompanyid(anyString(), anyString(), any(PageableMs.class))).thenReturn(page);
		ProjectDto project = new ProjectDto();
		project.setName("Test");
		when(projectRepository.findOneProject(anyString(), anyString())).thenReturn(project );
		when(invoiceLineService.getTotalPriceByInvoiceID(anyLong())).thenReturn(new BigDecimal("200"));
		List<InvoiceLineDto> result = service.getProjectsInvoiceLines("111", "2017-01");

		assertThat(new BigDecimal("200")).isEqualTo(result.get(0).getPrice());
		assertThat("Project Test").isEqualTo(result.get(0).getItemName());
	}

}
