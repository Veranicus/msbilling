package com.tieto.tds.dtos.invoicing;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceLineDto {

	private long id;

	private String no;

	private String itemName;

	private String unit;

	private Double amount;

	private BigDecimal price;

	private Date dateCreated;

	private Date lastUpdated;

	public InvoiceLineDto(){}

	public InvoiceLineDto(String itemName, String unit, Double amount, BigDecimal price) {
		super();
		this.itemName = itemName;
		this.unit = unit;
		this.amount = amount;
		this.price = price;
	}
	
	public InvoiceLineDto(String itemName, String unit, BigDecimal amount, Double price) {
		super();
		this.itemName = itemName;
		this.unit = unit;
		this.amount = amount.doubleValue();
		this.price = new BigDecimal(price);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	@Override
	public String toString() {
		return String.format("%s, %s, %s, %s", getItemName(), getAmount(), getUnit(), getPrice());
	}
}
