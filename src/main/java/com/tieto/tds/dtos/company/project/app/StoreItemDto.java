package com.tieto.tds.dtos.company.project.app;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tieto.tds.dtos.invoicing.InstanceType;

public class StoreItemDto {
	private String storeid;
	private String title;
	private String icon;
	private boolean aurora;
	private double price;
	private InstanceType type;

	private String description;
	private String usageInstruction;
	private String status;

	@JsonProperty("public")
	private boolean publik;

	private boolean real;

	private int positiveVotes;

	private Set<String> tags;

	public String getTitle() {
		if (title == null){
			return "N/A";
		}
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isAurora() {
		return aurora;
	}
	public void setAurora(boolean aurora) {
		this.aurora = aurora;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getStoreid() {
		return storeid;
	}
	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUsageInstruction() {
		return usageInstruction;
	}
	public void setUsageInstruction(String usageInstruction) {
		this.usageInstruction = usageInstruction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public InstanceType getType() {
		return type;
	}
	public void setType(InstanceType type) {
		this.type = type;
	}
	public boolean getPublik() {
		return publik;
	}
	public void setPublik(boolean publik) {
		this.publik = publik;
	}
	public boolean isReal() {
		return real;
	}
	public void setReal(boolean real) {
		this.real = real;
	}
	public int getPositiveVotes() {
		return positiveVotes;
	}
	public void setPositiveVotes(int positiveVotes) {
		this.positiveVotes = positiveVotes;
	}

}
