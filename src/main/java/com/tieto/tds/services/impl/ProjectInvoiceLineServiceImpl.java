package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.CustomInvoiceLine;
import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.ApplicationRepository;
import com.tieto.tds.repositories.CustomInvoiceLineRepository;
import com.tieto.tds.repositories.TenantUsageRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.ProjectInvoiceLineService;
import com.tieto.tds.services.UsageService;
import com.tieto.tds.services.commons.ResourceUsage;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class ProjectInvoiceLineServiceImpl implements ProjectInvoiceLineService{

	private static final Logger log = LoggerFactory.getLogger(ProjectInvoiceLineServiceImpl.class);

	private TenantUsageRepository tenantUsageRepository;

	private UsageService usageService;

	private ApplicationRepository applicationRepository;

	private InvoiceLineRepository invoiceLineRepository;

	private CustomInvoiceLineRepository customInvoiceLineRepository;

	@Autowired
	public ProjectInvoiceLineServiceImpl(TenantUsageRepository tenantUsageRepository,
			UsageService usageService, ApplicationRepository applicationRepository,
			InvoiceLineRepository invoiceLineRepository, CustomInvoiceLineRepository customInvoiceLineRepository) {
		super();
		this.tenantUsageRepository = tenantUsageRepository;
		this.usageService = usageService;
		this.applicationRepository = applicationRepository;
		this.invoiceLineRepository = invoiceLineRepository;
		this.customInvoiceLineRepository = customInvoiceLineRepository;
	}

	public void cleanupProjectinvoice(ProjectInvoice projectInvoice) {
		invoiceLineRepository.delete( projectInvoice.getLines() );
	}

	private void logResources(String tenantid, ResourceUsage resourceUsage){
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonInString = objectMapper.writeValueAsString(resourceUsage);
			log.debug("{} RETURNED: {}", tenantid, jsonInString);
		} catch (JsonProcessingException e) {
			log.warn("Error logging resources for tenantid {}.", tenantid, e);
		}
	}

	public List<InvoiceLineDto> getProjectResourcesInvoiceLines(List<ResourceDto> resources, FeeDto fee, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("getProjectResourcesInvoiceLines entered");
		List<InvoiceLineDto> result = new ArrayList<>();

		String tenantid = findOneTenantid(resources);
		if (tenantid == null) return result;

		List<Usage> tenantUsage = tenantUsageRepository.getUsageForTenantInTimePeriod(tenantid , periodFrom, periodTo);
		ResourceUsage totalUsage = usageService.getTotalUsage(tenantUsage);
		logResources(tenantid, totalUsage);

		result.add( new InvoiceLineDto("CPU", Constants.INVOICE_UNIT_PCS, totalUsage.getCpu(), fee.getCpu()) );
		result.add( new InvoiceLineDto("RAM", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getRam(), fee.getRam()) );
		result.add( new InvoiceLineDto("HDD", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getHdd(), fee.getHdd()) );
		result.add( new InvoiceLineDto("Instances", Constants.INVOICE_UNIT_PCS, totalUsage.getInstances(), fee.getInstance()) );
		result.add( new InvoiceLineDto("Images", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getImages(), fee.getHdd()) );
		result.add( new InvoiceLineDto("Snapshots", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getSnapshots(), fee.getHdd()) );
		result.add( new InvoiceLineDto("Volumes", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getVolumes(), fee.getHdd()) );

		return result;
	}

	private String findOneTenantid(List<ResourceDto> resources) {
		if (resources !=null && !resources.isEmpty()){
			if (resources.size()>1){
				log.warn("There are more tenants in project!! tenants: " + resources.get(0).getTenantid() + ", " + resources.get(1).getTenantid());
			}
			return resources.get(0).getTenantid();
		}
		return null;
	}

	public List<InvoiceLineDto> getProjectApplicationCostInvoiceLines(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("getProjectApplicationCostInvoiceLines {}, {}", companyid, projectid);

		List<InvoiceLineDto> result = new ArrayList<>();
		List<ApplicationDto> apps = applicationRepository.findAllActiveAppsByProjectidAndPeriod(companyid, projectid, periodFrom, periodTo);
		for (ApplicationDto app: apps){
			log.debug(app.getAppid() + " + " + app.getCostPeriod() + " for " + app.getCostPrice());
			InvoiceLineDto appInvoiceLine = new InvoiceLineDto();
			appInvoiceLine.setItemName( app.getStoreItem().getTitle() + " - " + app.getServer().getFqdn() );
			appInvoiceLine.setAmount(1d);
			appInvoiceLine.setPrice(BigDecimal.valueOf(app.getCostPrice()));
			appInvoiceLine.setUnit("pcs");
			result.add(appInvoiceLine);
		}
		return result;
	}

	public List<InvoiceLineDto> getProjectCustomInvoiceLines(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("getProjectCustomInvoiceLines {}, {}", companyid, projectid);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String key = periodFrom.format(formatter);

		List<InvoiceLineDto> result = new ArrayList<>();
		List<CustomInvoiceLine>  customLines = customInvoiceLineRepository.findAllByNoAndProjectid(key, projectid);
		for (CustomInvoiceLine customInvoiceLine : customLines) {
			InvoiceLineDto appInvoiceLine = new InvoiceLineDto();
			appInvoiceLine.setItemName(customInvoiceLine.getItemName());
			appInvoiceLine.setAmount(customInvoiceLine.getAmount());
			appInvoiceLine.setPrice(customInvoiceLine.getPrice());
			appInvoiceLine.setUnit(customInvoiceLine.getUnit());
			result.add(appInvoiceLine);
		}
		return result;
	}
}
