package com.tieto.tds.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.invoicing.CustomInvoiceLineDto;
import com.tieto.tds.services.CustomInvoiceLineService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags={"CustomInvoiceLine"})
public class ProjectCustomInvoiceLineController {

	private static final Logger log = LoggerFactory.getLogger(ProjectCustomInvoiceLineController.class);

	private CustomInvoiceLineService customInvoiceLineService;
	
	@Autowired
	public ProjectCustomInvoiceLineController(CustomInvoiceLineService customInvoiceLineService) {
		super();
		this.customInvoiceLineService = customInvoiceLineService;
	}

	@ApiOperation(value = "Create custom invoice line for project")
	@RequestMapping(path="/company/{companyid}/project/{projectid}/customInvoiceLine", method = RequestMethod.POST)
	public CustomInvoiceLineDto createCustomInvoiceLineForProject(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@RequestBody CustomInvoiceLineDto customInvoiceLine){
		return customInvoiceLineService.createCustomInvoiceLineForProject(companyid, projectid, customInvoiceLine);
	}
	
	@ApiOperation(value = "Find all custom invoice lines for project")
	@RequestMapping(path="/company/{companyid}/project/{projectid}/customInvoiceLine", method = RequestMethod.GET)
	public List<CustomInvoiceLineDto> findAllCustomInvoiceLineByProjectid(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@RequestParam(value="max", required=false, defaultValue="10") int max,
			@RequestParam(value="page", required=false, defaultValue="1") int page,
			@RequestParam(value="orderby", required=false, defaultValue="id") String orderby,
			@RequestParam(value="sort", required=false, defaultValue="ASC") PageSort sort,
			@RequestParam(value="number", required=false) String number){
		log.debug("/company/{companyid}/project/{projectid}/customInvoiceLine?max={}&page={}&orderby={}&sort={}&number={}",
				companyid, projectid, max, page, orderby, sort, number);

		return customInvoiceLineService.findAllCustomInvoiceLinesByProjectAndNo(companyid, projectid, number);
	}

	@ApiOperation(value = "Get one project custom invoiceline")
	@RequestMapping(path="/company/{companyid}/project/{projectid}/customInvoiceLine/{id}", method = RequestMethod.GET)
	public CustomInvoiceLineDto getOneCustomInvoiceLine(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@PathVariable("projectid") long id){
		log.debug("/company/{companyid}/project/{projectid}/customInvoiceLine/{id}",
				companyid, projectid, id);

		return customInvoiceLineService.findOneCustomInvoiceLine(companyid, projectid, id);
	}
}
