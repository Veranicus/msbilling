package com.tieto.tds.configs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="portal")
public class BillingProperties {

	private String reportfolder;

	public String getReportfolder() {
		return reportfolder;
	}

	public void setReportfolder(String reportfolder) {
		this.reportfolder = reportfolder;
	}

}
