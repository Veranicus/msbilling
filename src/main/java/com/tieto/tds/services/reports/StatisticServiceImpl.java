package com.tieto.tds.services.reports;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.reporting.CapacityStatisticsDto;

@Repository
@Profile("default")
public class StatisticServiceImpl implements StatisticService{

	private static final Logger log = LoggerFactory.getLogger(StatisticServiceImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Map<String, CapacityStatisticsDto> getAllProjectsUsageForPeriod(String number) {
		String query = "select i.number, i.projectid, i.companyid, l.itemname, l.amount from b_project_invoice i, b_invoice_line l where number = ? and i.id = l.invoice_id";

		Map<String, CapacityStatisticsDto> result = new HashMap<>();

		List<ProjectInvoiceExportLine> results = jdbcTemplate.query(
				query, new Object[] { number },
                (rs, rowNum) -> new ProjectInvoiceExportLine(rs.getString("number"), rs.getString("projectid"),
                        rs.getString("companyid"), rs.getString("itemname"),rs.getDouble("amount"))
        );
		logUsage(query, results);

		for (ProjectInvoiceExportLine row: results){
			String projectid = row.getProjectid();
			CapacityStatisticsDto capacity = result.get(projectid);
			if (capacity == null) {
				capacity = new CapacityStatisticsDto();
				capacity.setProjectid(projectid);
				capacity.setCompanyid(row.getCompanyid());
				result.put(projectid, capacity);
			}
			Double amount = (Double)row.getAmount();
			String itemName = row.getItemname();
			switch(itemName){
				case Constants.USAGE_RAM:
					capacity.setRam(amount);
					break;
				case Constants.USAGE_DISK:
					capacity.setDisk(amount);
					break;
				case "HDD":
					capacity.setDisk(amount);
					break;
				case Constants.USAGE_CPU:
					capacity.setCpu(amount);
					break;
				case Constants.USAGE_INSTANCES:
					capacity.setInstance(amount);
					break;
				case Constants.USAGE_SNAPSHOT:
					capacity.setSnapshot(amount);
					break;
				case Constants.USAGE_VOLUMES:
					capacity.setVolume(amount);
					break;
				case Constants.USAGE_IMAGES:
					capacity.setImage(amount);
					break;
				default:
					log.warn("Unsupported usage row {} with amount {}", itemName, amount);
			}
		}


		return result;
	}

	private void logUsage(String query, List<ProjectInvoiceExportLine> results) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonInString = objectMapper.writeValueAsString(results);
			log.debug("Project usage query = {}, result = {}", query, jsonInString);
		} catch (JsonProcessingException e) {
			log.warn("Error logging usage.", e);
		}
	}

	@Override
	public BigDecimal getTotalPrice(List<InvoiceLine> lines){
		BigDecimal totalPrice = BigDecimal.ZERO;
		if (lines != null && !lines.isEmpty()){
			for (InvoiceLine line : lines) {
				BigDecimal subTotal = line.getPrice().multiply(new BigDecimal(line.getAmount()));
				totalPrice = totalPrice.add(subTotal);
			}
		}
		return totalPrice;
	}
}
