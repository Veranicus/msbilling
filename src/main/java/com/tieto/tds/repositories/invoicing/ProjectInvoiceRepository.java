package com.tieto.tds.repositories.invoicing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.tieto.tds.domains.invoice.ProjectInvoice;

public interface ProjectInvoiceRepository extends JpaRepository<ProjectInvoice, Long>{

	public ProjectInvoice findOneByNumberAndProjectid(String number, String projectid);

	public List<ProjectInvoice> findAllByProjectid(String projectid);

	public Page<ProjectInvoice> findAllByProjectid(String projectid, Pageable pageable);

	public ProjectInvoice findOneById(long id);

	public List<ProjectInvoice> findAllByCompanyid(String companyid);
	
	public Page<ProjectInvoice> findAllByCompanyid(String companyid, Pageable pageable);

	public Page<ProjectInvoice> findAllByCompanyidAndNumber(String companyid, String number, Pageable pageable);

	public List<ProjectInvoice> findAllByCompanyidAndNumber(String companyid, String number);

	public List<ProjectInvoice> findAllByNumber(String number);

}
