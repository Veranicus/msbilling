package com.tieto.tds.domains.invoice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.line.InvoiceLine;

@RunWith(MockitoJUnitRunner.class)
public class ProjectInvoiceTest {


	@Test
	public void test(){
		ProjectInvoice invoice = new ProjectInvoice();
		invoice.setCompanyid("111");
		invoice.setProjectid("222");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		List<InvoiceLine> lines =  new ArrayList<InvoiceLine>();
		invoice.setLines(lines);
		invoice.setNumber("2017-01");

		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("222").isEqualTo(invoice.getProjectid());
		assertThat("2017-01").isEqualTo(invoice.getNumber());
		assertNotNull(invoice.getDateCreated());
		assertThat(0).isEqualTo(invoice.getLines().size());
		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("111").isEqualTo(invoice.getCompanyid());
	}
}
