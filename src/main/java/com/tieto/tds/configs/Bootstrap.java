package com.tieto.tds.configs;

import java.util.TimeZone;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tieto.tds.repositories.BackendRepository;

@Component
public class Bootstrap implements InitializingBean{

	private BackendRepository backendRepository;
	
	@Autowired
	public Bootstrap(BackendRepository backendRepository) {
		super();
		this.backendRepository = backendRepository;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

		backendRepository.doLogin();
	}

}
