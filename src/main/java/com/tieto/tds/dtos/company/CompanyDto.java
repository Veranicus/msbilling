package com.tieto.tds.dtos.company;

public class CompanyDto {

	private String title;

	private String areaid;

	private String sharedProductid;

	private double userFee;

	private String feeType;

	private double oncallFee;

	private String domain;

	public CompanyDto() {
		super();
	}

	public CompanyDto(String title, String areaid, String sharedProductid, double userFee,
			String feeType, double oncallFee, String domain) {
		super();
		this.title = title;
		this.areaid = areaid;
		this.sharedProductid = sharedProductid;
		this.userFee = userFee;
		this.feeType = feeType;
		this.oncallFee = oncallFee;
		this.domain = domain;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAreaid() {
		return areaid;
	}

	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}

	public double getUserFee() {
		return userFee;
	}

	public void setUserFee(double userFee) {
		this.userFee = userFee;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public double getOncallFee() {
		return oncallFee;
	}

	public void setOncallFee(double oncallFee) {
		this.oncallFee = oncallFee;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSharedProductid() {
		return sharedProductid;
	}

	public void setSharedProductid(String sharedProductid) {
		this.sharedProductid = sharedProductid;
	}

}
