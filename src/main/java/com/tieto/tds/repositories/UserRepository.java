package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.user.UserDto;

public interface UserRepository {

	public List<UserDto> findALlUsersByCompanyid(String companid);

	public List<UserDto> findAllUsersByCompanyidAndCostCenterTitle(String companyid, String cctitle);

	public UserDto getOneUser(String username);
}
