package com.tieto.tds.services.reports;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;

@Service
public class ReportHeaderServiceImpl implements ReportHeaderService {

	private static final Logger log = LoggerFactory.getLogger(ReportHeaderServiceImpl.class);

	private static final String TOTAL_PRICE_HEADER = "Total price";
	@Override
	public Row createUsageHeader(Row row) {
		row.createCell(0).setCellValue("Project");
		row.createCell(1).setCellValue("ID");
		row.createCell(2).setCellValue("CPU");
		row.createCell(3).setCellValue("RAM");
		row.createCell(4).setCellValue("DISK");
		row.createCell(5).setCellValue("INSTANCES");
		row.createCell(6).setCellValue("SNAPSHOTS");
		row.createCell(7).setCellValue("IMAGES");
		row.createCell(8).setCellValue("VOLUMES");
		row.createCell(9).setCellValue("Errors");
		return row;
	}

	@Override
	public Row createProjectHeader(Row row) {
		row.createCell(0).setCellValue("Project");
		row.createCell(1).setCellValue("ID");
		row.createCell(2).setCellValue("Cost center");
		row.createCell(3).setCellValue(TOTAL_PRICE_HEADER);
		row.createCell(4).setCellValue("Tier");
		row.createCell(5).setCellValue("Company");
		row.createCell(6).setCellValue("Link");
		row.createCell(7).setCellValue("Errors");
		return row;
	}

	@Override
	public Row createHeaderForCostCenters(Row row) {
		row.createCell(0).setCellValue("Cost center");
		row.createCell(1).setCellValue("Company");
		row.createCell(2).setCellValue(TOTAL_PRICE_HEADER);
		row.createCell(3).setCellValue("Link");
		return row;
	}

	@Override
	public Row createHeaderForCompany(Row row) {
		row.createCell(0).setCellValue("Title");
		row.createCell(1).setCellValue(TOTAL_PRICE_HEADER);
		row.createCell(2).setCellValue("Link");
		return row;
	}

	@Override
	public void createCostCenterFullReportHeader(Row row, Row subheader, CompanyDto company, List<AurApp> aurApps) {
		log.debug("Create createCostCenterFullReportHeader for company {}", company);
		row.createCell(0).setCellValue("Cost Center");
		row.createCell(1).setCellValue("Users");
		subheader.createCell(1).setCellValue(company.getUserFee());
		if (aurApps!=null && !aurApps.isEmpty()){
			int i = 2;
			for (AurApp app : aurApps) {
				row.createCell(i).setCellValue(app.getFqdn());
				subheader.createCell(i++).setCellValue(app.getBilling().getValue());
			}
		}
	}

}
