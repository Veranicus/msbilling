package com.tieto.tds.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import com.tieto.tds.dtos.invoicing.CompanyInvoiceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.services.CompanyInvoiceService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.commons.LocalDateHolder;

public class CompanyBillingControllerTest {

	MockMvc mockMvc;

	@Mock
	private CompanyInvoiceService companyInvoiceService;

	@Mock
	private LocalDateUtilService localDateUtilService;

	@InjectMocks
	private CompanyBillingController controller = new CompanyBillingController(
			companyInvoiceService, localDateUtilService);
	

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();
	}	

	@Test
	public void thatCreateCompayInvoicesAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(null, null)).thenReturn(value );
		mockMvc.perform(post("/company/111/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatCreateCompayInvoicesWithFromParameterAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(any(LocalDate.class), any(null))).thenReturn(value );

		mockMvc.perform(post("/company/111/invoice?from=2017-11-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	public void thatCreateCompayInvoicesWithFromAndToParameterAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(any(LocalDate.class), any(LocalDate.class))).thenReturn(value );

		mockMvc.perform(post("/company/111/invoice?from=2017-11-01&to=2017-12-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test(expected = NestedServletException.class) 
	public void thatCreateCompayInvoicesWithFromAndToParameterAPIReturns400() throws Exception {
		mockMvc.perform(post("/company/111/invoice?from=2017-12-01&to=2017-11-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatGetOneCompanyInvoiceAPIReturns200() throws Exception {
		CompanyInvoiceDto companyInvoiceDto = new CompanyInvoiceDto();
		companyInvoiceDto.setCompanyid("111");
		companyInvoiceDto.setDateCreated(Calendar.getInstance().getTime());
		companyInvoiceDto.setId(1);
		companyInvoiceDto.setNumber("2017-01");
		List<InvoiceLineDto> lines = new ArrayList<InvoiceLineDto>();
		InvoiceLineDto line = new InvoiceLineDto();
		line.setAmount(1d);
		line.setDateCreated(Calendar.getInstance().getTime());
		line.setId(1);
		line.setItemName("name");
		line.setLastUpdated(Calendar.getInstance().getTime());
		line.setNo("2017-01");
		line.setPrice(new BigDecimal(10));
		line.setUnit("pcs");
		companyInvoiceDto.setLines(lines );
		when(companyInvoiceService.findOne(anyString(), anyLong())).thenReturn(companyInvoiceDto );
		mockMvc.perform(get("/company/111/invoice/222"))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	public void thatFindAllCompanyInvoicesAPIReturns200() throws Exception {
		CompanyInvoiceDto companyInvoiceDto = new CompanyInvoiceDto();
		companyInvoiceDto.setCompanyid("111");
		companyInvoiceDto.setDateCreated(Calendar.getInstance().getTime());
		companyInvoiceDto.setId(1);
		companyInvoiceDto.setNumber("2017-01");
		List<InvoiceLineDto> lines = new ArrayList<InvoiceLineDto>();
		InvoiceLineDto line = new InvoiceLineDto();
		line.setAmount(1d);
		line.setDateCreated(Calendar.getInstance().getTime());
		line.setId(1);
		line.setItemName("name");
		line.setLastUpdated(Calendar.getInstance().getTime());
		line.setNo("2017-01");
		line.setPrice(new BigDecimal(10));
		line.setUnit("pcs");
		lines.add(line);
		companyInvoiceDto.setLines(lines );
		List<CompanyInvoiceDto> companyInvoicesDto = new ArrayList<CompanyInvoiceDto>();
		companyInvoicesDto.add(companyInvoiceDto);
		when(companyInvoiceService.findAllByCompanyid(anyString())).thenReturn(companyInvoicesDto  );
		mockMvc.perform(get("/company/111/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}
}
