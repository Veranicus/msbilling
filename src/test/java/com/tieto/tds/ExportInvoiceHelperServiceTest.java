package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.services.reports.ExportInvoiceHelperServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ExportInvoiceHelperServiceTest {

	ExportInvoiceHelperServiceImpl service = new ExportInvoiceHelperServiceImpl();

	@Test
	public void test_getUserCountForCC() {

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		InvoiceLine line1 = new InvoiceLine();
		line1.setItemName("Cost center user fee");
		line1.setAmount(3d);
		lines.add(line1);
		double userCountForCC = service.getUserCountForCC(lines);

		assertThat(userCountForCC).isEqualTo(3d);
	}

	@Test
	public void test_getUserCountForCCWithNoMatchinName() {

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		InvoiceLine line1 = new InvoiceLine();
		line1.setItemName("Blah blah");
		line1.setAmount(3d);
		lines.add(line1);
		double userCountForCC = service.getUserCountForCC(lines);

		assertThat(userCountForCC).isEqualTo(0d);
	}

	@Test
	public void test_getUserCountForCCWithNoLines() {

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		double userCountForCC = service.getUserCountForCC(lines);

		assertThat(userCountForCC).isEqualTo(0d);
	}

	@Test
	public void test_getUserCountForCCWithNullLines() {

		double userCountForCC = service.getUserCountForCC(null);

		assertThat(userCountForCC).isEqualTo(0d);
	}

	@Test
	public void test_getAurUsersCount(){
		AurApp aurApp = new AurApp();
		aurApp.setFqdn("testlink.etb.tieto.com");

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		InvoiceLine line1 = new InvoiceLine();
		line1.setItemName("Testlink - testlink.etb.tieto.com users");
		line1.setAmount(3d);
		lines.add(line1);

		double aurUsersCount = service.getAurUsersCount(aurApp, lines);

		assertThat(aurUsersCount).isEqualTo(3d);
	}

	@Test
	public void test_getAurUsersCountWithNoLines(){
		AurApp aurApp = new AurApp();
		aurApp.setFqdn("testlink.etb.tieto.com");

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();

		double aurUsersCount = service.getAurUsersCount(aurApp, lines);

		assertThat(aurUsersCount).isEqualTo(0d);
	}

	@Test
	public void test_getAurUsersCountWithNullLines(){
		AurApp aurApp = new AurApp();
		aurApp.setFqdn("testlink.etb.tieto.com");

		double aurUsersCount = service.getAurUsersCount(aurApp, null);

		assertThat(aurUsersCount).isEqualTo(0d);
	}

	@Test
	public void test_getAurUsersCountWithNoMathinglines(){
		AurApp aurApp = new AurApp();
		aurApp.setFqdn("testlink.etb.tieto.com");

		List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
		InvoiceLine line1 = new InvoiceLine();
		line1.setItemName("Testlink - testlinknomatch.etb.tieto.com users");
		line1.setAmount(3d);
		lines.add(line1);

		double aurUsersCount = service.getAurUsersCount(aurApp, lines);

		assertThat(aurUsersCount).isEqualTo(0d);
	}
}
