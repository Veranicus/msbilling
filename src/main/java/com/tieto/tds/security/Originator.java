package com.tieto.tds.security;

public interface Originator {

	public void setOriginator(String userToken);

	public String getOriginator();

}
