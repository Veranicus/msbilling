package com.tieto.tds.dtos.invoicing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class InvoiceDto {

	private long id;

	private String number;

	private Date dateCreated;

	private List<InvoiceLineDto> lines;

	public BigDecimal getTotalPrice(){
		BigDecimal totalPrice = BigDecimal.ZERO;
		if (getLines() != null && !getLines().isEmpty()){
			for (InvoiceLineDto line : getLines()) {
				BigDecimal subTotal = line.getPrice().multiply(new BigDecimal(line.getAmount()));
				totalPrice = totalPrice.add(subTotal);
			}
		}
		return totalPrice;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<InvoiceLineDto> getLines() {
		return lines;
	}

	public void setLines(List<InvoiceLineDto> lines) {
		this.lines = lines;
	}

	public void addLines(List<InvoiceLineDto> lines) {
		if (this.lines==null){
			this.lines = new ArrayList<>();
		}
		this.lines.addAll(lines);
	}

	public void addLines(InvoiceLineDto line) {
		if (this.lines==null){
			this.lines = new ArrayList<>();
		}
		this.lines.add(line);
	}

}
