package com.tieto.tds.repositories.invoicing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.tieto.tds.domains.invoice.CostCenterInvoice;

public interface CostCenterInvoiceRepository extends JpaRepository<CostCenterInvoice, Long>{

	CostCenterInvoice findOneByNumberAndCostCenterId(String number, String costCenterId);

	CostCenterInvoice findOneById(long id);

	List<CostCenterInvoice> findAllByCompanyidAndNumber(String companyid, String number);

	List<CostCenterInvoice> findAllByNumber(String number);

	Page<CostCenterInvoice> findAllByCompanyid(String companyid, Pageable pageableRequest);

	public Page<CostCenterInvoice> findAllByCompanyidAndCostCenterId(String companyid, String costCenterId, Pageable pageable);

	public Page<CostCenterInvoice> findAllByCompanyidAndNumber(String companyid, String no, Pageable pageableRequest);
}
