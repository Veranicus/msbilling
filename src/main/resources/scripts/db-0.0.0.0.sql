CREATE TABLE b_project_invoice
(
  id bigint NOT NULL,
  date_created timestamp without time zone,
  "number" character varying(255),
  companyid character varying(255),
  projectid character varying(255),
  CONSTRAINT b_project_invoice_pkey PRIMARY KEY (id)
);
CREATE TABLE b_costcenter_invoice
(
  id bigint NOT NULL,
  date_created timestamp without time zone,
  "number" character varying(255),
  companyid character varying(255),
  costcenterid character varying(255),
  CONSTRAINT b_costcenter_invoice_pkey PRIMARY KEY (id)
);
CREATE TABLE b_company_invoice
(
  id bigint NOT NULL,
  date_created timestamp without time zone,
  no character varying(255),
  period timestamp without time zone,
  total_price numeric(19,2),
  companyid character varying(255),
  "number" character varying(255),
  CONSTRAINT b_company_invoice_pkey PRIMARY KEY (id)
);
CREATE TABLE b_invoice_line
(
  id bigint NOT NULL,
  amount double precision,
  dateureated timestamp without time zone,
  itemname character varying(255),
  lastupdated timestamp without time zone,
  price numeric(20,10),
  unit character varying(255),
  invoice_id bigint,
  CONSTRAINT b_invoice_line_pkey PRIMARY KEY (id)
);
CREATE TABLE b_custom_invoice_line
(
  id bigint NOT NULL,
  amount double precision,
  companyid character varying(255),
  cost_center_id character varying(255),
  date_created timestamp without time zone,
  item_name character varying(255),
  last_updated timestamp without time zone,
  no character varying(255),
  price numeric(19,2),
  projectid character varying(255),
  unit character varying(255),
  CONSTRAINT b_custom_invoice_line_pkey PRIMARY KEY (id)
);
CREATE SEQUENCE hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 87723
  CACHE 1;