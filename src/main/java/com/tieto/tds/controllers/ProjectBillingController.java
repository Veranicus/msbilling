package com.tieto.tds.controllers;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.InvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.ProjectInvoiceService;
import com.tieto.tds.services.commons.LocalDateHolder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags={"Project"})
public class ProjectBillingController {

	private static final Logger log = LoggerFactory.getLogger(ProjectBillingController.class);

	private ProjectInvoiceService projectInvoiceService;

	private LocalDateUtilService localDateUtilService;

	public ProjectBillingController(ProjectInvoiceService projectInvoiceService,
			LocalDateUtilService localDateUtilService) {
		super();
		this.projectInvoiceService = projectInvoiceService;
		this.localDateUtilService = localDateUtilService;
	}

	@ApiOperation(value = "Create project invoice for specific period")
	@RequestMapping(path="/company/{companyid}/project/{projectid}/invoice", method = RequestMethod.POST)
	public InvoiceDto createProjectInvoiceByDate(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@RequestParam(value="from", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodFrom,
			@RequestParam(value="to", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodTo){
		log.info("periodFrom = {}, periodTo = {}", periodFrom, periodTo);
		LocalDateHolder localDateHolder = localDateUtilService.getFromAndToFromInput(periodFrom, periodTo);

		return projectInvoiceService.createProjectInvoice(companyid, projectid, localDateHolder.getFrom(), localDateHolder.getTo());
	}

	@ApiOperation(value = "Get project invoice by id")
	@RequestMapping(
			value="/company/{companyid}/project/{projectid}/invoice/{id}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			method = RequestMethod.GET)
	public InvoiceDto findOneProjectInvoiceById(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@PathVariable("id") long id){
		return projectInvoiceService.findOne(companyid, projectid, id);
	}

	@ApiOperation(value = "get all project invoice")
	@RequestMapping(path="/company/{companyid}/project/{projectid}/invoice", method = RequestMethod.GET,
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ProjectInvoicePageDto findAllProjectInvoicesByProjectid(
			@PathVariable("companyid") String companyid,
			@PathVariable("projectid") String projectid,
			@RequestParam(value="max", required=false, defaultValue="10") int max,
			@RequestParam(value="page", required=false, defaultValue="1") int page,
			@RequestParam(value="orderby", required=false, defaultValue="id") String orderby,
			@RequestParam(value="sort", required=false, defaultValue="DESC") PageSort sort){
		PageableMs pageable = new PageableMs(page, max, sort, orderby);
		return projectInvoiceService.findAllByProjectid(companyid, projectid, pageable);
	}

	@ApiOperation(value = "get all project invoice by company")
	@RequestMapping(path="/company/{companyid}/project/invoice", method = RequestMethod.GET,
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ProjectInvoicePageDto findAllProjectInvoicesByCompanyid(
			@PathVariable("companyid") String companyid,
			@RequestParam(value="no", required=false) String no,
			@RequestParam(value="max", required=false, defaultValue="10") int max,
			@RequestParam(value="page", required=false, defaultValue="1") int page,
			@RequestParam(value="orderby", required=false, defaultValue="id") String orderby,
			@RequestParam(value="sort", required=false, defaultValue="DESC") PageSort sort){
		PageableMs pageable = new PageableMs(page, max, sort, orderby);
		return projectInvoiceService.findAllByCompanyid(companyid, no, pageable);
	}
}
