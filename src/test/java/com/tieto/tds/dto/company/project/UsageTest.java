package com.tieto.tds.dto.company.project;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.dtos.company.project.resource.Usage;

@RunWith(MockitoJUnitRunner.class)
public class UsageTest {

	@Test
	public void testNoNullReturned() throws JsonProcessingException{
		Usage server = new Usage();
		server.setVolumeSnapshots(111);
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(server);

		assertThat(jsonInString, containsString("volume_snapshots"));
	}
}
