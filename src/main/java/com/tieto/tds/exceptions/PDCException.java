package com.tieto.tds.exceptions;

public class PDCException extends RuntimeException {

	private static final long serialVersionUID = 4934091206361057775L;

	public PDCException(String string) {
		super(string);
	}

}
