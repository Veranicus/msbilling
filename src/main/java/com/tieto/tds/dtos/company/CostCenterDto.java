package com.tieto.tds.dtos.company;

public class CostCenterDto {

	private long id;

	private String title;

	private CompanyDto company;

	private boolean active;

	public CostCenterDto() {
		super();
	}

	public CostCenterDto(long id, String title, CompanyDto company, boolean active) {
		super();
		this.id = id;
		this.title = title;
		this.company = company;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CompanyDto getCompany() {
		if (company != null){
			return company;
		} else {
			return new CompanyDto("NA", "NA", "NA", 0d, "NA", 0d, "NA");
		}
	}

	public void setCompany(CompanyDto company) {
		this.company = company;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
