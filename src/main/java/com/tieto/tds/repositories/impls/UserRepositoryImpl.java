package com.tieto.tds.repositories.impls;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.common.UserPageable;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository{

	private static final Logger log = LoggerFactory.getLogger(UserRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public UserRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}
	
	@Override
	public List<UserDto> findALlUsersByCompanyid(String companyid) {
		String url = getAllUsersByCompanyidUrl(companyid);
		UserPageable users = restTemplate.getForObject(url, UserPageable.class);
		return users.getContent();
	}

	@Override
	public List<UserDto> findAllUsersByCompanyidAndCostCenterTitle(String companyid, String cctitle) {
		String url = getAllUsersByCompanyidAndCostCenterTitleUrl(companyid, cctitle);
		UserPageable users = restTemplate.getForObject(url, UserPageable.class);
		return users.getContent();

	}

	@Override
	public UserDto getOneUser(String username){
		String url = getOneUserUrl(username);

		return restTemplate.getForObject(url, UserDto.class);
	}

	private String getAllUsersByCompanyidUrl(String companyid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/users?attrs=globalRoles&max=100000").buildAndExpand(companyid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	public String getAllUsersByCompanyidAndCostCenterTitleUrl(String companyid, String cctitle) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/costcenters/{cctitle}/users?max=100000").buildAndExpand(companyid, cctitle);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	public String getOneUserUrl(String username){
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/users/{username}").buildAndExpand(username);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}
}
