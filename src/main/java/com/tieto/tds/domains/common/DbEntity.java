package com.tieto.tds.domains.common;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DbEntity implements Serializable {

	private static final long serialVersionUID = 1365779736419388536L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
}