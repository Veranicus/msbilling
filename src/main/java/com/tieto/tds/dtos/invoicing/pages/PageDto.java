package com.tieto.tds.dtos.invoicing.pages;

import com.tieto.tds.dtos.common.Pageable;

public class PageDto {

	private Pageable pageable;

	public Pageable getPageable() {
		return pageable;
	}

	public void setPageable(Pageable pageable) {
		this.pageable = pageable;
	}
}
