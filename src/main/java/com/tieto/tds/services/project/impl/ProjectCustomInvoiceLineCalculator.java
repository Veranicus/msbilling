package com.tieto.tds.services.project.impl;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tieto.tds.domains.invoice.line.CustomInvoiceLine;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.CustomInvoiceLineRepository;
import com.tieto.tds.services.DateTimeService;
import com.tieto.tds.services.project.ProjectInvoiceLineCalculator;
import com.tieto.tds.services.project.ProjectInvoiceRequest;

@Service("projectCustomInvoiceLineCalculator")
public class ProjectCustomInvoiceLineCalculator extends ProjectInvoiceLineCalculator {

	private static final Logger log = LoggerFactory.getLogger(ProjectCustomInvoiceLineCalculator.class);

	private CustomInvoiceLineRepository customInvoiceLineRepository;

	private DateTimeService dateTimeService;

	private Mapper mapper;

	@Autowired
	public ProjectCustomInvoiceLineCalculator(CustomInvoiceLineRepository customInvoiceLineRepository,
			DateTimeService dateTimeService, Mapper mapper) {
		super();
		this.customInvoiceLineRepository = customInvoiceLineRepository;
		this.dateTimeService = dateTimeService;
		this.mapper = mapper;
	}

	@Override
	public List<InvoiceLineDto> getProjectInvoiceLines(ProjectInvoiceRequest request) {
		log.debug("getProjectInvoiceLines {}", request);
		String invoiceNumber = dateTimeService.getInvoiceNumberFromStartDate(request.getPeriodFrom());

		List<InvoiceLineDto> result = new ArrayList<>();
		List<CustomInvoiceLine>  customLines = customInvoiceLineRepository.findAllByNoAndProjectid(invoiceNumber, request.getProjectid());
		for (CustomInvoiceLine customInvoiceLine : customLines) {
			InvoiceLineDto appInvoiceLine = mapper.map( customInvoiceLine, InvoiceLineDto.class);
			result.add(appInvoiceLine);
		}
		return result;
	}

}
