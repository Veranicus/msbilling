package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.company.project.ProjectDto;

public interface ProjectRepository {

	public ProjectDto findOneProject(String companyid, String projectid);
	
	public List<ProjectDto> findAllProjectsByCompany(String companyid);
}
