package com.tieto.tds.controllers;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;
import com.tieto.tds.services.CostCenterInvoiceService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.commons.LocalDateHolder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags={"CostCenter"})
public class CostCenterBillingController {

	private static final Logger log = LoggerFactory.getLogger(CostCenterBillingController.class);

	private CostCenterInvoiceService costCenterInvoiceService;

	private LocalDateUtilService localDateUtilService;

	@Autowired
	public CostCenterBillingController(CostCenterInvoiceService costCenterInvoiceService,
			LocalDateUtilService localDateUtilService) {
		super();
		this.costCenterInvoiceService = costCenterInvoiceService;
		this.localDateUtilService = localDateUtilService;
	}

	@ApiOperation(value = "Create cost center invoice for specific period")
	@RequestMapping(path="/company/{companyid}/costcenter/{cctitle}/invoice", method = RequestMethod.POST)
	public CostCenterInvoiceDto createProjectInvoiceByDate(
			@PathVariable("companyid") String companyid,
			@PathVariable("cctitle") String cctitle,
			@RequestParam(value="from", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodFrom,
			@RequestParam(value="to", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodTo){
		log.debug("create invoices for {}, {}, from: {}, to: {}.", companyid, cctitle, periodFrom, periodTo);

		LocalDateHolder localDateHolder = localDateUtilService.getFromAndToFromInput(periodFrom, periodTo);

		return costCenterInvoiceService.createCostCenterInvoiceByDate(
				companyid, cctitle, localDateHolder.getFrom(), localDateHolder.getTo());
	}

	@ApiOperation(value = "Get cost center invoice by id")
	@RequestMapping(path="/company/{companyid}/costcenter/{cctitle}/invoice/{id}", method = RequestMethod.GET)
	public CostCenterInvoiceDto findProjectInvoiceByPeriod(
			@PathVariable("companyid") String companyid,
			@PathVariable("cctitle") String cctitle,
			@PathVariable("id") long id){
		return costCenterInvoiceService.findOne(companyid, cctitle, id);
	}

	@ApiOperation(value = "get all project invoice")
	@RequestMapping(path="/company/{companyid}/costcenter/{cctitle}/invoice", method = RequestMethod.GET)
	public CostCenterInvoicePageDto findAllCostCenterInvoicesByCostCenter(
			@PathVariable("companyid") String companyid,
			@PathVariable("cctitle") String cctitle,
			@RequestParam(value="max", required=false, defaultValue="10") int max,
			@RequestParam(value="page", required=false, defaultValue="1") int page,
			@RequestParam(value="orderby", required=false, defaultValue="id") String orderby,
			@RequestParam(value="sort", required=false, defaultValue="DESC") PageSort sort){
		PageableMs pageable = new PageableMs(page, max, sort, orderby);
		return costCenterInvoiceService.findAllByCostCenter(companyid, cctitle, pageable);
	}
	
	@ApiOperation(value = "get all CC invoice by compamny")
	@RequestMapping(path="/company/{companyid}/costcenter/invoice", method = RequestMethod.GET)
	public CostCenterInvoicePageDto findAllCostCenterInvoicesByCompany(
			@PathVariable("companyid") String companyid,
			@RequestParam(value="max", required=false, defaultValue="10") int max,
			@RequestParam(value="page", required=false, defaultValue="1") int page,
			@RequestParam(value="orderby", required=false, defaultValue="id") String orderby,
			@RequestParam(value="sort", required=false, defaultValue="DESC") PageSort sort,
			@RequestParam(value="number", required=false, defaultValue="") String number){
		PageableMs pageable = new PageableMs(page, max, sort, orderby);
		if (number != "" && number.length()>0){
			return costCenterInvoiceService.findAllByCompanyidAndNo(companyid, number, pageable);
		} else {
			return costCenterInvoiceService.findAllByCompanyid(companyid, pageable);
		}
	}
}
