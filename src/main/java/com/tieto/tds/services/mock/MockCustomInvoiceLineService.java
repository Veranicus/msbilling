package com.tieto.tds.services.mock;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.invoicing.CustomInvoiceLineDto;
import com.tieto.tds.services.CustomInvoiceLineService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockCustomInvoiceLineService implements CustomInvoiceLineService{

	@Override
	public CustomInvoiceLineDto createCustomInvoiceLineForProject(String companyid, String projectid,
			CustomInvoiceLineDto customInvoiceLine) {
		return new CustomInvoiceLineDto();
	}

	@Override
	public CustomInvoiceLineDto findOneCustomInvoiceLine(String companyid, String projectid, long id) {
		return new CustomInvoiceLineDto();
	}

	@Override
	public List<CustomInvoiceLineDto> findAllCustomInvoiceLinesByProjectAndNo(String companyid, String projectid, String no) {
		List<CustomInvoiceLineDto> invoices = new ArrayList<>();
		CustomInvoiceLineDto invoice = new CustomInvoiceLineDto();
		invoices.add(invoice);
		return invoices;
	}

}
