package com.tieto.tds.dtos.invoicing;

public class ProjectInvoiceDto extends InvoiceDto{

	private String companyid;

	private String projectid;

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

}
