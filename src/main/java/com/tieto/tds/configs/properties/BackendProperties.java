package com.tieto.tds.configs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="backend")
public class BackendProperties {

	private String fqdn;

	private String schema;

	private String username;

	private String password;

	private boolean useorchestration;

	public String getFqdn() {
		return fqdn;
	}

	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isUseorchestration() {
		return useorchestration;
	}

	public void setUseorchestration(boolean useorchestration) {
		this.useorchestration = useorchestration;
	}

}
