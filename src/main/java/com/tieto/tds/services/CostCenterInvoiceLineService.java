package com.tieto.tds.services;

import java.util.List;

import com.tieto.tds.dtos.invoicing.InvoiceLineDto;

public interface CostCenterInvoiceLineService {

	List<InvoiceLineDto> getSaaSInvoiceLines(String companyid, String cctitle, String no);

	List<InvoiceLineDto> getSaaSUserFeeInvoiceLines(String companyid, String cctitle);

}
