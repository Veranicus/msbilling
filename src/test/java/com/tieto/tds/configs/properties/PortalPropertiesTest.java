package com.tieto.tds.configs.properties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PortalPropertiesTest {

	@Test
	public void test(){
		PortalProperties prop = new PortalProperties();
		prop.setContext("portal");
		prop.setUsername("username");
		prop.setPassword("pwd");
		
		assertThat("portal").isEqualTo(prop.getContext());
		assertThat("username").isEqualTo(prop.getUsername());
		assertThat("pwd").isEqualTo(prop.getPassword());
	}
}
