package com.tieto.tds.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.line.CustomInvoiceLine;
import com.tieto.tds.dtos.invoicing.CustomInvoiceLineDto;
import com.tieto.tds.exceptions.PDCException;
import com.tieto.tds.repositories.CustomInvoiceLineRepository;
import com.tieto.tds.services.CustomInvoiceLineService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CustomInvoiceLineServiceImpl implements CustomInvoiceLineService{

	private CustomInvoiceLineRepository customInvoiceLineRepository;

	@Autowired
	public CustomInvoiceLineServiceImpl(CustomInvoiceLineRepository customInvoiceLineRepository) {
		super();
		this.customInvoiceLineRepository = customInvoiceLineRepository;
	}

	@Override
	public CustomInvoiceLineDto createCustomInvoiceLineForProject(String companyid, String projectid,
			CustomInvoiceLineDto customInvoiceLine) {
		CustomInvoiceLine line = new CustomInvoiceLine();
		line.setItemName(customInvoiceLine.getItemName());
		line.setAmount(customInvoiceLine.getAmount());
		line.setNo(customInvoiceLine.getNo());
		line.setPrice(customInvoiceLine.getPrice());
		line.setProjectid(projectid);
		line.setCompanyid(companyid);
		line.setUnit(customInvoiceLine.getUnit());

		CustomInvoiceLine savedLine = customInvoiceLineRepository.save(line);
		customInvoiceLine.setId(savedLine.getId());
		return customInvoiceLine;
	}

	@Override
	public CustomInvoiceLineDto findOneCustomInvoiceLine(String companyid, String projectid, long id) {
		throw new PDCException("Not implemented yet.");
	}

	@Override
	public List<CustomInvoiceLineDto> findAllCustomInvoiceLinesByProjectAndNo(String companyid, String projectid, String no) {
		List<CustomInvoiceLine> customInvoiceLines;
		if (no == null){
			customInvoiceLines = customInvoiceLineRepository.findAllByProjectid(projectid);
		} else {
			customInvoiceLines = customInvoiceLineRepository.findAllByNoAndProjectid(no, projectid);
		}

		List<CustomInvoiceLineDto> result = new ArrayList<>();
		for (CustomInvoiceLine customInvoiceLine : customInvoiceLines) {
			CustomInvoiceLineDto line = new CustomInvoiceLineDto();
			line.setAmount(customInvoiceLine.getAmount());
			line.setCompanyid(customInvoiceLine.getCompanyid());
			line.setDateCreated(customInvoiceLine.getDateCreated());
			line.setId(customInvoiceLine.getId());
			line.setItemName(customInvoiceLine.getItemName());
			line.setLastUpdated(customInvoiceLine.getLastUpdated());
			line.setNo(customInvoiceLine.getNo());
			line.setPrice(customInvoiceLine.getPrice());
			line.setProjectid(customInvoiceLine.getProjectid());
			line.setUnit(customInvoiceLine.getUnit());
			result.add(line);
		}
		return result;
	}

}
