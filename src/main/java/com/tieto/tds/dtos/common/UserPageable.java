package com.tieto.tds.dtos.common;

import java.util.List;

import com.tieto.tds.dtos.user.UserDto;

public class UserPageable extends Pageable{

	private List<UserDto> content;

	public List<UserDto> getContent() {
		return content;
	}

	public void setContent(List<UserDto> content) {
		this.content = content;
	}
}
