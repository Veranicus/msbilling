package com.tieto.tds.dto.invoicing;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;

@RunWith(MockitoJUnitRunner.class)
public class ProjectInvoiceDtoTest {

	@Test
	public void testTotalPrice(){
		ProjectInvoiceDto  invoice = getInvoice();
		
		
		assertThat(new BigDecimal("122.31360")).isEqualTo(invoice.getTotalPrice());
	}

	private ProjectInvoiceDto getInvoice() {
		ProjectInvoiceDto  invoice = new ProjectInvoiceDto();
		invoice.setCompanyid("111");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		invoice.setId(1);
		List<InvoiceLineDto> lines = new ArrayList<InvoiceLineDto>();
		lines.add(getInvoiceLine(1, 2976, "CPU", "2017-01", new BigDecimal("0.00877"), "pcs"));
		lines.add(getInvoiceLine(2, 5952, "RAM", "2017-01", new BigDecimal("0.00731"), "pcs"));
		lines.add(getInvoiceLine(3, 74400, "HDD", "2017-01", new BigDecimal("0.00015"), "pcs"));
		lines.add(getInvoiceLine(4, 1488, "instances", "2017-01", new BigDecimal("0.02792"), "pcs"));
		invoice.setLines(lines);
		return invoice;
	}

	private InvoiceLineDto getInvoiceLine(long id, double amount, String itemName, String no, BigDecimal price, String unit) {
		InvoiceLineDto line1 = new InvoiceLineDto();
		line1.setAmount(amount);
		line1.setDateCreated(Calendar.getInstance().getTime());
		line1.setId(id);
		line1.setItemName(itemName);
		line1.setLastUpdated(Calendar.getInstance().getTime());
		line1.setNo(no);
		line1.setPrice(price);
		line1.setUnit(unit);
		return line1;
	}

}
