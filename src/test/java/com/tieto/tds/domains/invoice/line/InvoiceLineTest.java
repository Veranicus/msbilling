package com.tieto.tds.domains.invoice.line;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceLineTest {

	@Test
	public void test(){
		InvoiceLine line = new InvoiceLine();
		line.setAmount(1d);
		line.setDateCreated(Calendar.getInstance().getTime());
		line.setId(1);
		line.setItemName("name");
		line.setLastUpdated(Calendar.getInstance().getTime());
		line.setPrice(new BigDecimal(10));
		line.setUnit("pcs");
		
		assertThat(1d).isEqualTo(line.getAmount());
		assertNotNull(line.getDateCreated());
		assertThat(1l).isEqualTo(line.getId());
		assertThat("name").isEqualTo(line.getItemName());
		assertNotNull(line.getLastUpdated());
		assertThat(new BigDecimal(10)).isEqualTo(line.getPrice());
		assertThat("pcs").isEqualTo(line.getUnit());
	}
}
