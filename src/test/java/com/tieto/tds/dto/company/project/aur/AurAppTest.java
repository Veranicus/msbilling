package com.tieto.tds.dto.company.project.aur;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.aur.AurApp;

@RunWith(MockitoJUnitRunner.class)
public class AurAppTest {

	@Test
	public void testTotalPrice(){
		AurApp app = new AurApp();
		
		assertNotNull(app.getBilling());
		
		assertThat(app.getBilling().getUserSuppoertFee(), is(0d));
		assertThat(app.getBilling().getValue(), is(0d));
	}

}
