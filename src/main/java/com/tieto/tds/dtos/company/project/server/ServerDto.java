package com.tieto.tds.dtos.company.project.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.dtos.company.project.app.ConnectionDto;
import com.tieto.tds.dtos.company.project.resource.CapacityDto;
import com.tieto.tds.dtos.user.UserDto;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerDto {

	private String serverid;
	private String fqdn;
	private String status;

	private String authentication;
	private boolean manageable;

	private OsDto os;
	private CapacityDto capacity;
	private List<ApplicationDto> applications;
	private List<ConnectionDto> connections;

	private Date dateCreated;
	private UserDto createdBy;

	public String getServerid() {
		return serverid;
	}
	public void setServerid(String serverid) {
		this.serverid = serverid;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public String getHostname(){
		if (this.fqdn==null || 
				this.fqdn.length()==0){
			return "N/A";
		}
		int pos = this.fqdn.indexOf('.');
		if (pos > 0){
			return this.fqdn.substring(0, pos);
		} else {
			return "N/A";
		}
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAuthentication() {
		return authentication;
	}
	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}
	public OsDto getOs() {
		if (os == null){
			return new OsDto();
		}
		return os;
	}
	public void setOs(OsDto os) {
		this.os = os;
	}
	public List<ApplicationDto> getApplications() {
		if (applications == null){
			return new ArrayList<>();
		}
		return applications;
	}
	public void setApplications(List<ApplicationDto> applications) {
		this.applications = applications;
	}
	public boolean isManageable() {
		return manageable;
	}
	public void setManageable(boolean manageable) {
		this.manageable = manageable;
	}
	public CapacityDto getCapacity() {
		if (capacity == null){
			return new CapacityDto();
		}
		return capacity;
	}
	public void setCapacity(CapacityDto capacity) {
		this.capacity = capacity;
	}
	public List<ConnectionDto> getConnections() {
		if (connections == null){
			return new ArrayList<>();
		}
		return connections;
	}
	public void setConnections(List<ConnectionDto> connections) {
		this.connections = connections;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public UserDto getCreatedBy() {
		if (createdBy == null){
			return new UserDto();
		}
		return createdBy;
	}
	public void setCreatedBy(UserDto createdBy) {
		this.createdBy = createdBy;
	}

}
