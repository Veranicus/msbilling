package com.tieto.tds.services;

import java.time.LocalDate;

import com.tieto.tds.services.commons.LocalDateHolder;

public interface LocalDateUtilService {

	/**
	 *
	 * @param from
	 * @param to
	 * @return both days, if 'to' is missing return first day of next month.
	 * if 'from' is missing return first day of this month.
	 */
	public LocalDateHolder getFromAndToFromInput(LocalDate from, LocalDate to);
}
