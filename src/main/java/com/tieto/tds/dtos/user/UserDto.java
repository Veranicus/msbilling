package com.tieto.tds.dtos.user;

import java.util.Date;
import java.util.Set;

import com.tieto.tds.dtos.company.UserRoleDto;


public class UserDto {

	private long id;

	private String userToken;

	private String firstname;

	private String lastname;

	private String fullname;

	private String email;

	private String departmentNumber;

	private Date lastLogin;

	private Date lastLoginLdap;

	private Date registrationDate;

	private String password;

	private boolean enabled;

	private Set<UserRoleDto> globalRoles;

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getLastLoginLdap() {
		return lastLoginLdap;
	}

	public void setLastLoginLdap(Date lastLoginLdap) {
		this.lastLoginLdap = lastLoginLdap;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "Usertoken: "+getUserToken()+", firstname: " + getFirstname() + ", lastname: "+getLastname() + ", email:" + getEmail();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserRoleDto> getGlobalRoles() {
		return globalRoles;
	}

	public void setGlobalRoles(Set<UserRoleDto> globalRoles) {
		this.globalRoles = globalRoles;
	}
}
