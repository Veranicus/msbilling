package com.tieto.tds.dto.company.project;

import static org.junit.Assert.assertNotNull;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.TierDto;

@RunWith(MockitoJUnitRunner.class)
public class TierDtoTest {

	@Test
	public void testNotNull1(){
		TierDto tier = new TierDto();

		assertNotNull(tier.getCurrentFee());
	}

	@Test
	public void testNotNull2(){
		TierDto tier = new TierDto();
		tier.setCurrentFee(new FeeDto(1, Calendar.getInstance().getTime(), 0d, 0d, 0d, 0d, 0d, 0d));
		assertNotNull(tier.getCurrentFee());
	}

}
