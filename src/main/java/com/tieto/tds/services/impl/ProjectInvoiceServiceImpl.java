package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.repositories.invoicing.ProjectInvoiceRepository;
import com.tieto.tds.services.DateTimeService;
import com.tieto.tds.services.ProjectInvoiceService;
import com.tieto.tds.services.project.ProjectInvoiceLineCalculator;
import com.tieto.tds.services.project.ProjectInvoiceRequest;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class ProjectInvoiceServiceImpl implements ProjectInvoiceService {

	private static final Logger log = LoggerFactory.getLogger(ProjectInvoiceServiceImpl.class);

	private ProjectInvoiceRepository invoiceRepository;

	private InvoiceLineRepository invoiceLineRepository;

	private Mapper mapper;

	private ProjectInvoiceLineCalculator projectInvoiceLineCalculator;

	private DateTimeService dateTimeService;

	@Autowired
	public ProjectInvoiceServiceImpl(ProjectInvoiceRepository invoiceRepository,
			InvoiceLineRepository invoiceLineRepository, Mapper mapper,
			List<ProjectInvoiceLineCalculator> projectInvoiceLineCalculators,
			DateTimeService dateTimeService) {
		super();
		this.invoiceRepository = invoiceRepository;
		this.invoiceLineRepository = invoiceLineRepository;
		this.mapper = mapper;
		this.dateTimeService = dateTimeService;

		if (projectInvoiceLineCalculators!=null){
			for (ProjectInvoiceLineCalculator calculator : projectInvoiceLineCalculators) {
				if (projectInvoiceLineCalculator != null){
					calculator.setSuccessor(projectInvoiceLineCalculator);
				}
				projectInvoiceLineCalculator = calculator;
			}
		}
	}

	@Override
	public ProjectInvoiceDto createProjectInvoice(String companyid, String projectid, LocalDate periodFrom,  LocalDate periodTo) {
		log.debug("createProjectInvoice {}, {}", companyid, projectid);
		ProjectInvoiceRequest request = new ProjectInvoiceRequest(companyid, projectid, periodFrom, periodTo);

		List<InvoiceLineDto> projectInvoiceLines = projectInvoiceLineCalculator.getInvoiceLines(request);

		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		invoice.addLines(projectInvoiceLines);

		String invoiceNumber = dateTimeService.getInvoiceNumberFromStartDate(periodFrom);

		ProjectInvoice projectInvoice = invoiceRepository.findOneByNumberAndProjectid(invoiceNumber, projectid);
		if (projectInvoice != null) {
			invoiceRepository.delete(projectInvoice);
		}

		projectInvoice = new ProjectInvoice(companyid, projectid, invoiceNumber);
		ProjectInvoice savedProjectInvoice = invoiceRepository.saveAndFlush(projectInvoice);

		for (InvoiceLineDto lineDto : invoice.getLines()) {
			InvoiceLine line = new InvoiceLine(projectInvoice, lineDto.getItemName(), lineDto.getUnit(), lineDto.getAmount(), lineDto.getPrice());
			InvoiceLine savedInstance = invoiceLineRepository.saveAndFlush(line);
			log.debug("Invoie line was saved {}, {}, {}.", invoiceNumber, projectid, savedInstance.getId());
		}

		log.debug("Project invoice was saved {}, {}, {}.", invoiceNumber, projectid, savedProjectInvoice.getId());

		invoice.setId(savedProjectInvoice.getId());
		invoice.setDateCreated(savedProjectInvoice.getDateCreated());
		invoice.setNumber(invoiceNumber);
		invoice.setCompanyid(companyid);
		invoice.setProjectid(projectid);
		return invoice;
	}

	@Override
	public ProjectInvoiceDto findOne(String companyid, String projectid, long id) {
		ProjectInvoice invoice = invoiceRepository.findOneById(id);
		return mapper.map(invoice, ProjectInvoiceDto.class);
	}

	@Override
	public ProjectInvoicePageDto findAllByProjectid(String companyid, String projectid, PageableMs pageable) {
		Pageable pageableRequest = getPageable(pageable); 
		Page<ProjectInvoice> projectInvoices = invoiceRepository.findAllByProjectid(projectid, pageableRequest );
		return mapInvoices(projectInvoices);
	}

	@Override
	public ProjectInvoicePageDto findAllByCompanyid(String companyid, String no, PageableMs pageable) {
		Pageable pageableRequest = getPageable(pageable); 
		
		Page<ProjectInvoice> projectInvoices;
		if (no !=null){
			projectInvoices = invoiceRepository.findAllByCompanyidAndNumber(companyid, no, pageableRequest);
		} else {
			projectInvoices = invoiceRepository.findAllByCompanyid(companyid, pageableRequest);
		}
		return mapInvoices(projectInvoices);
	}

	private Pageable getPageable(PageableMs pageable) {
		org.springframework.data.domain.Sort.Direction sortDirection = org.springframework.data.domain.Sort.Direction.DESC;
		if (pageable.getSort() == PageSort.ASC ) sortDirection = org.springframework.data.domain.Sort.Direction.ASC;
		return new PageRequest(pageable.getPage()-1, pageable.getMax(), sortDirection, pageable.getOrderby());
	}

	/**
	 * map project invoice with invoice lines from DB to DTO object
	 * @param projectInvoices - paged object with DB invoices
	 * @return DTO representation of invoice with invoice lines
	 */
	private ProjectInvoicePageDto mapInvoices(Page<ProjectInvoice> projectInvoices) {
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		List<ProjectInvoiceDto> result = new ArrayList<>();
		for (ProjectInvoice projectInvoice : projectInvoices) {
			ProjectInvoiceDto invoiceDto = new ProjectInvoiceDto();
			invoiceDto.setCompanyid(projectInvoice.getCompanyid());
			invoiceDto.setDateCreated(projectInvoice.getDateCreated());
			invoiceDto.setId(projectInvoice.getId());
			invoiceDto.setNumber(projectInvoice.getNumber());
			invoiceDto.setProjectid(projectInvoice.getProjectid());

			List<InvoiceLineDto> lines = getLines(projectInvoice);
			invoiceDto.setLines(lines );

			result.add(invoiceDto);
		}
		com.tieto.tds.dtos.common.Pageable pageResult = new com.tieto.tds.dtos.common.Pageable();
		pageResult.setLast(projectInvoices.isLast());
		pageResult.setNumber(projectInvoices.getNumber());
		pageResult.setNumberOfElements(projectInvoices.getNumberOfElements());
		pageResult.setSize(projectInvoices.getSize());
		pageResult.setTotalPages(projectInvoices.getTotalPages());
		page.setPageable(pageResult);
		page.setContent(result);
		return page;
	}

	/**
	 * map DB invoice lines of provided project invoice into DTO object
	 * @param projectInvoice is requested invoice
	 * @return DTO representation of invoice lines
	 */
	private List<InvoiceLineDto> getLines(ProjectInvoice projectInvoice) {
		List<InvoiceLine> lines = invoiceLineRepository.findAllByInvoice(projectInvoice);
		List<InvoiceLineDto> linesDto = new ArrayList<>();
		for (InvoiceLine invoiceLine : lines) {
			InvoiceLineDto lineDto = new InvoiceLineDto();
			lineDto.setAmount(invoiceLine.getAmount());
			lineDto.setDateCreated(invoiceLine.getDateCreated());
			lineDto.setId(invoiceLine.getId());
			lineDto.setItemName(invoiceLine.getItemName());
			lineDto.setLastUpdated(invoiceLine.getLastUpdated());
			lineDto.setNo("");
			lineDto.setPrice(invoiceLine.getPrice());
			lineDto.setUnit(invoiceLine.getUnit());
			linesDto.add(lineDto);
		}
		return linesDto;
	}

}
