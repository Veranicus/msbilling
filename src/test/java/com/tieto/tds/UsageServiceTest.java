package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.services.commons.ResourceUsage;
import com.tieto.tds.services.impl.UsageServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UsageServiceTest {

	UsageServiceImpl service = new UsageServiceImpl();
	
	@Test
	public void test_Single_getTotalUsage(){
		List<Usage> tenantUsage = getSingleUsage();

		ResourceUsage totalUsage = service.getTotalUsage(tenantUsage);

		assertEquals(new BigDecimal("5208.00000000000000000000"), totalUsage.getCpu());
	}

	@Test
	public void test_Multi_getTotalUsage(){
		List<Usage> tenantUsage = getMultiUsage();

		ResourceUsage totalUsage = service.getTotalUsage(tenantUsage);

		assertEquals(new BigDecimal("2436.00000000000000000000"), totalUsage.getCpu());
	}

	private List<Usage> getSingleUsage() {
		List<Usage> tenantUsage = new ArrayList<Usage>();
		Usage usage = new Usage();
		usage.setImages(0);
		usage.setInstances(2);
//		Date timestamp = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
//		try {
//			timestamp = sdf.parse("00:00:00 01/10/2017");
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		LocalDateTime timestamp = LocalDateTime.of(2017, Month.OCTOBER, 01, 00, 00, 00);
		usage.setTimestamp(timestamp );
		usage.setVcpu(7);
		usage.setVdisk(40);
		usage.setVolumeSnapshots(0);
		usage.setVolumes(0);
		usage.setVram(8704);
		tenantUsage.add(usage);
		return tenantUsage;
	}
	
	private List<Usage> getMultiUsage() {
		List<Usage> tenantUsage = new ArrayList<Usage>();
		Usage usage = new Usage();
		usage.setImages(0);
		usage.setInstances(2);
		LocalDateTime timestamp = LocalDateTime.of(2017, Month.OCTOBER, 01, 00, 00, 00);

		usage.setTimestamp(timestamp );
		usage.setVcpu(7);
		usage.setVdisk(40);
		usage.setVolumeSnapshots(0);
		usage.setVolumes(0);
		usage.setVram(8704);
		
		Usage usage2 = new Usage();
		usage2.setImages(0);
		usage2.setInstances(0);
		LocalDateTime timestamp2 = LocalDateTime.of(2017, Month.OCTOBER, 15, 12, 00, 00); 
		usage2.setTimestamp(timestamp2 );
		usage2.setVcpu(0);
		usage2.setVdisk(0);
		usage2.setVolumeSnapshots(0);
		usage2.setVolumes(0);
		usage2.setVram(0);

		tenantUsage.add(usage);
		tenantUsage.add(usage2);

		return tenantUsage;
	}
}
