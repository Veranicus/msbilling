package com.tieto.tds.repositories.common;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LoginResponse {

	private String token;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="HH:mm:ss dd/MM/yyyy")
	private Date expiration;

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getExpiration() {
		return expiration;
	}
	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

}
