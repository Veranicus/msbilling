package com.tieto.tds.configs;

public class Constants {

	public static final String PROFILE_DEFAULT = "default";
	public static final String PROFILE_MOCK = "mock";

	public static final String PDCADMIN = "pdcadmin";

	public static final String BACKEND_TIME_FORMAT = "HH:mm:ss dd/MM/yyyy";

	public static final String TIME_PERIOD_FORMAT = "yyyy-MM";

	public static final String USAGE_INSTANCES= "INSTANCES";
	public static final String USAGE_CPU = "CPU";
	public static final String USAGE_RAM = "RAM";
	public static final String USAGE_DISK = "DISK";
	public static final String USAGE_IMAGES = "Images";
	public static final String USAGE_SNAPSHOT = "Snapshots";
	public static final String USAGE_VOLUMES = "Volumes";
	public static final String INVOICE = "Project";
	public static final String COST_CENTER = "Cost Center";
	public static final String USERS = "Users";
	public static final String ON_CALL= "On call";
	public static final String SERVICE = "SaaS";
	public static final String COSTCENTER_USER_FEE = "Cost center user fee";

	public static final String INVOICE_UNIT_GB_HOURS = "GB hour";
	public static final String INVOICE_UNIT_PCS = "pcs";

	private Constants(){}
}
