package com.tieto.tds.dtos.company.project;

import java.util.Date;

public class FeeDto {

	private long id;

	private Date validFrom;

	private double instance;

	private double cpu;

	private double ram;

	private double hdd;

	private double ssd;

	private double network;

	public FeeDto() {
		super();
	}

	public FeeDto(long id, Date validFrom, double instance, double cpu,
			double ram, double hdd, double ssd, double network) {
		super();
		this.id = id;
		this.validFrom = validFrom;
		this.instance = instance;
		this.cpu = cpu;
		this.ram = ram;
		this.hdd = hdd;
		this.ssd = ssd;
		this.network = network;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public double getInstance() {
		return instance;
	}

	public void setInstance(double instance) {
		this.instance = instance;
	}

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}

	public double getRam() {
		return ram;
	}

	public void setRam(double ram) {
		this.ram = ram;
	}

	public double getHdd() {
		return hdd;
	}

	public void setHdd(double hdd) {
		this.hdd = hdd;
	}

	public double getSsd() {
		return ssd;
	}

	public void setSsd(double ssd) {
		this.ssd = ssd;
	}

	public double getNetwork() {
		return network;
	}

	public void setNetwork(double network) {
		this.network = network;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
