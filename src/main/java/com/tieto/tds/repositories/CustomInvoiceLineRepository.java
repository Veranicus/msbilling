package com.tieto.tds.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tieto.tds.domains.invoice.line.CustomInvoiceLine;

public interface CustomInvoiceLineRepository extends JpaRepository<CustomInvoiceLine, Long>{

	List<CustomInvoiceLine> findAllByNoAndProjectid(String no, String projectid);

	List<CustomInvoiceLine> findAllByProjectid(String projectid);

}
