package com.tieto.tds.services;

public interface CacheManager {

	public void onProjectid(String projectid);

	public void onCompanyid(String compandyid);

}
