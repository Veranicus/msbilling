package com.tieto.tds.dtos.reporting;

public class CapacityStatisticsDto {

	private String projectid;
	private String companyid;

	private double ram;
	private double disk;
	private double cpu;
	private double instance;
	private double snapshot;
	private double volume;
	private double image;

	public double getRam() {
		return ram;
	}
	public void setRam(double ram) {
		this.ram = ram;
	}
	public double getDisk() {
		return disk;
	}
	public void setDisk(double disk) {
		this.disk = disk;
	}
	public double getCpu() {
		return cpu;
	}
	public void setCpu(double cpu) {
		this.cpu = cpu;
	}
	public double getInstance() {
		return instance;
	}
	public void setInstance(double instance) {
		this.instance = instance;
	}
	public double getSnapshot() {
		return snapshot;
	}
	public void setSnapshot(double snapshot) {
		this.snapshot = snapshot;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getImage() {
		return image;
	}
	public void setImage(double image) {
		this.image = image;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	
}
