package com.tieto.tds.services;

import java.time.LocalDate;
import java.util.List;

import com.tieto.tds.dtos.invoicing.CompanyInvoiceDto;

public interface CompanyInvoiceService {

	public CompanyInvoiceDto createInvoice(String companyid, LocalDate periodFrom, LocalDate periodTo);

	public CompanyInvoiceDto findOne(String companyid, long id);

	public List<CompanyInvoiceDto> findAllByCompanyid(String companyid);
}
