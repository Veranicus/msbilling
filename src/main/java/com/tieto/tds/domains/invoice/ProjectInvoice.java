package com.tieto.tds.domains.invoice;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="b_project_invoice")
@DiscriminatorValue("project")
public class ProjectInvoice extends Invoice{

	private static final long serialVersionUID = 5490590778231409664L;

	@Column
	private String companyid;

	@Column
	private String projectid;

	public ProjectInvoice(){
		super();
	}

	public ProjectInvoice(String companyid, String projectid, String invoiceNumber) {
		super();
		super.setNumber(invoiceNumber);
		this.companyid = companyid;
		this.projectid = projectid;
	}

	@Override
	public String toString() {
		return super.toString() + ", projectid: " + projectid;
	}

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
}