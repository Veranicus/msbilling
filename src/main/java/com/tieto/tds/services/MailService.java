package com.tieto.tds.services;

import java.io.File;

public interface MailService {

	public void sendBillingReport(File file);
}
