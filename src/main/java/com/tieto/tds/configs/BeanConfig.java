package com.tieto.tds.configs;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.properties.MailProperties;
import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.repositories.common.BackendAuthenticationInterceptor;
import com.tieto.tds.repositories.common.BasicAurhInterceptor;
import com.tieto.tds.repositories.common.TokenHolder;
import com.tieto.tds.repositories.common.TokenHolderImpl;
import com.tieto.tds.security.Originator;
import com.tieto.tds.security.OriginatorImpl;

@Configuration
public class BeanConfig {

	private static final Logger log = LoggerFactory.getLogger(BeanConfig.class);

	@Autowired
	private PortalProperties properties;

	@Autowired
	private MailProperties mailProperties;

	@Bean
	UriComponentsBuilder uriComponentsBuilder(){
		return UriComponentsBuilder.newInstance();
	}

	@Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
          new ConcurrentMapCache("project"), 
          new ConcurrentMapCache("company")));
        return cacheManager;
    }

	@Bean
	public Mapper mapper(){
		return new DozerBeanMapper();
	}

	@Bean
	public ObjectMapper objectMapper(){
		return new ObjectMapper();
	}

	@Bean
	public RestTemplate backendRestTemplate() {

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Collections.singletonList(backendAuthenticationInterceptor()));
		return restTemplate;
	}

	@Bean
	public RestTemplate portalRestTemplate() {
		spoilSecurity();

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Collections.singletonList(portalBasicAuthInterceptor()));
		return restTemplate;
	}

	@Bean
	public Originator originator(){
		return new OriginatorImpl();
	}

	@Bean
	public TokenHolder tokenHolder(){
		return new TokenHolderImpl();
	}

	@Bean
	public ClientHttpRequestInterceptor backendAuthenticationInterceptor() {
		return new BackendAuthenticationInterceptor(originator(), tokenHolder());
	}

	@Bean
	public ClientHttpRequestInterceptor portalBasicAuthInterceptor() {
		log.debug("Mediator basic auth credentials: " + properties.getUsername() +" - "+ properties.getUsername());
		return new BasicAurhInterceptor(properties.getUsername(), properties.getPassword());
	}

	private void spoilSecurity() {
		HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> hostname.contains("tieto.com"));

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
			@Override
			public X509Certificate[] getAcceptedIssuers(){
				return new X509Certificate[]{};
			}
			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType){
				// Do nothing
			}
			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType){
				// Do nothing
			}
		}};

		try {
			SSLContext sslcontext = SSLContext.getInstance("TLS");
			sslcontext.init(null, trustAllCerts, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
		} catch (Exception e){
			log.warn("Error initializing ssl connection", e);
		}
	}

	@Bean
	public JavaMailSender mailSender(){
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailProperties.getHost());
		return mailSender;
	}
}
